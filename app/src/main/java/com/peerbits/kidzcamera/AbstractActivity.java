/**
 * Nov 19, 2014 8:38:07 AM
 */
package com.peerbits.kidzcamera;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.peerbits.kidzcamera.config.Config;
import com.peerbits.kidzcamera.config.Const;

/**
 * @author Cong
 *
 */
public class AbstractActivity extends ActionBarActivity {

	protected Config mConfig;
	protected SharedPreferences mPrefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mConfig = new Config(this);
		mPrefs = getSharedPreferences(Const.MAIN_CONTEXT, MODE_PRIVATE);
	}
	
	public Config getConfig() {
		return mConfig;
	}
	
	public SharedPreferences getSharedPreferences() {
		return mPrefs;
	}

    public KidzApplication getMyApplication() {
        return (KidzApplication) getApplication();
    }
}
