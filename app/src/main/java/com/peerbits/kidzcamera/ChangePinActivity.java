package com.peerbits.kidzcamera;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.fragment.OldPinFragment;


public class ChangePinActivity extends AbstractActivity {

    private Option mOption;
    private String mCurrentTag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);

        if (savedInstanceState == null) {
            Fragment fragment = OldPinFragment.newInstance();
            mCurrentTag = fragment.getClass().getName();
            getFragmentManager().beginTransaction().add(R.id.changePin_layoutContainer, fragment, mCurrentTag).commit();
        } else {
            mCurrentTag = savedInstanceState.getString(Const.ARG_ACTIVITY_CURRENT_TAG);
            Log.w("mCurrentTag", mCurrentTag);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(Const.ARG_ACTIVITY_CURRENT_TAG, mCurrentTag);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentTag = savedInstanceState.getString(Const.ARG_ACTIVITY_CURRENT_TAG);
    }

    @Override
    public void onBackPressed() {
        if (popFragment())
            return;
        super.onBackPressed();
    }

    public Option getOption() {
        return mOption;
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (addToBackStack) {
            mCurrentTag = fragment.getClass().getName();
            transaction.addToBackStack(mCurrentTag);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.replace(R.id.changePin_layoutContainer, fragment, mCurrentTag + "");
        transaction.commit();
    }

    public boolean popFragment() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            return true;
        }
        return false;
    }

    public boolean popAllFragment() {
        return getFragmentManager().getBackStackEntryCount() > 0 && getFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void saveNewPin(String pin) {
        mOption.setParentPin(pin);
        mPrefs.edit().putString(Const.PREF_OPTIONS, new Gson().toJson(mOption)).apply();
        setResult(RESULT_OK);
        Toast.makeText(this, R.string.message_save_pin_success, Toast.LENGTH_SHORT).show();
        finish();
    }
}
