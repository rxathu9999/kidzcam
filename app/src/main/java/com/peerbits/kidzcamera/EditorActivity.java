package com.peerbits.kidzcamera;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.dialog.BrushDialog;
import com.peerbits.kidzcamera.dialog.EraserDialog;
import com.peerbits.kidzcamera.dialog.PaletteDialog;
import com.peerbits.kidzcamera.utils.Utils;
import com.peerbits.kidzcamera.widget.DrawingPanel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;


public class EditorActivity extends AbstractActivity implements View.OnClickListener, PaletteDialog.PaletteDialogListener {

    private static final int REQ_SAVE_PHOTO = 1;

    private DrawingPanel mDrawView;
    private LinearLayout mLayController;
    private ProgressBar mPrgLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        ImageView mBtnPaint = (ImageView) findViewById(R.id.editor_btnPaint);
        ImageView mBtnPalette = (ImageView) findViewById(R.id.editor_btnPalette);
        ImageView mBtnEraser = (ImageView) findViewById(R.id.editor_btnEraser);
        ImageView mBtnUndo = (ImageView) findViewById(R.id.editor_btnUndo);
        ImageView mBtnDelete = (ImageView) findViewById(R.id.editor_btnDelete);
        ImageView mBtnSave = (ImageView) findViewById(R.id.editor_btnSave);
        mDrawView = (DrawingPanel) findViewById(R.id.editor_drawView);
        mLayController = (LinearLayout) findViewById(R.id.editor_layoutController);
        mPrgLoading = (ProgressBar) findViewById(R.id.editor_prgLoading);

        mBtnPaint.setOnClickListener(this);
        mBtnPalette.setOnClickListener(this);
        mBtnEraser.setOnClickListener(this);
        mBtnUndo.setOnClickListener(this);
        mBtnDelete.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);

        new AsyncTask<Integer, Integer, Bitmap>() {
            @Override
            protected void onPreExecute() {
                mPrgLoading.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap doInBackground(Integer... integers) {
                Bitmap bitmap = Utils.decodeImageFile(mConfig.getTempPhoto().getAbsolutePath(), mConfig.getScreenWidth(), mConfig.getScreenHeight());
                return Utils.convertToMutable(bitmap, mConfig.getBmpTempDir());
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                System.gc();
                mDrawView.setPhotoBitmap(bitmap);
                mPrgLoading.setVisibility(View.GONE);
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editor_btnPaint:
                mDrawView.setMode(DrawingPanel.MODE_BRUSH);
                BrushDialog.newInstance().show(getFragmentManager());
                break;
            case R.id.editor_btnPalette:
                mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down_out));
                PaletteDialog.newInstance().show(getFragmentManager());
                break;
            case R.id.editor_btnEraser:
                mDrawView.setMode(DrawingPanel.MODE_ERASER);
                EraserDialog.newInstance().show(getFragmentManager());
                break;
            case R.id.editor_btnUndo:
                mDrawView.undo();
                break;
            case R.id.editor_btnDelete:
//                Intent intent = new Intent(this, SaveActivity.class);
//                startActivityForResult(intent, REQ_SAVE_PHOTO);
                Option option = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
                if (option.getPhotoLimit() == 0) {
                    finish();
                } else if (getStorageCount() >= option.getPhotoLimit()) {
                    File[] files = mConfig.getMediaDir().listFiles();
                    Arrays.sort(files);
                    if (files.length > 0)
                        files[0].delete();
                    savePhoto();
                } else {
                    savePhoto();
                }
                break;
            case R.id.editor_btnSave:
//                Intent intent = new Intent(this, SaveActivity.class);
//                startActivityForResult(intent, REQ_SAVE_PHOTO);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_SAVE_PHOTO) {
            if (resultCode == RESULT_OK) {
                savePhoto();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPicked(int color) {
        mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_in));
        mDrawView.setColor(color);
    }

    @Override
    public void onDismissed(DialogFragment dialog) {
        if (dialog instanceof PaletteDialog)
            mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_in));
    }

    private void savePhoto() {
        new AsyncTask<Integer, Integer, Boolean>() {
            @Override
            protected void onPreExecute() {
                mPrgLoading.setVisibility(View.VISIBLE);
            }

            @Override
            protected Boolean doInBackground(Integer... integers) {
                String s = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(new Date()) + ".jpg";
                mDrawView.saveTo(new File(mConfig.getMediaDir(), s));
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                mPrgLoading.setVisibility(View.GONE);
//                Toast.makeText(EditorActivity.this, getString(R.string.message_photo_save_success), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(EditorActivity.this, GalleryActivity.class);
                startActivity(intent);
                finish();
            }
        }.execute();
    }


    private int getStorageCount() {
        File[] files = mConfig.getMediaDir().listFiles();
//        int count = 0;
//        for (File file : files) {
//            String path = file.getAbsolutePath();
//            String ext = path.substring(path.lastIndexOf(".")).toLowerCase();
//            if (ext.equals(".jpg"))
//                count += 1;
//        }
//        return count;
        return files.length;
    }

}
