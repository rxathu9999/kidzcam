package com.peerbits.kidzcamera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.peerbits.kidzcamera.adapter.GalleryAdapter;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class GalleryActivity extends AbstractActivity {

    public static final int REQ_EDIT_PHOTO = 21;
    public static final String EXTRA_FILE = "GalleryActivity.EXTRA_FILE";

    private Option mOption;
    private Toolbar mToolbar;

    private RecyclerView mRclMain;
    private ProgressBar mPrgLoading;
    private GalleryAdapter mAdapter;
    private List<File> mFiles = new ArrayList<>();

    private int mColumnCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_gallery);
        mToolbar.setNavigationIcon(R.drawable.ic_camera);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRclMain = (RecyclerView) findViewById(R.id.gallery_recyclerView);
        mPrgLoading = (ProgressBar) findViewById(R.id.gallery_prgLoading);

        mColumnCount = getResources().getInteger(R.integer.gallery_num_column);

        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        mRclMain.setLayoutManager(new GridLayoutManager(this, mColumnCount));
        mRclMain.addItemDecoration(new GridItemSpacingDecorator(mColumnCount, px));
        mAdapter = new GalleryAdapter(this, mFiles, mConfig.getThumbDir());
        mRclMain.setAdapter(mAdapter);

        LinearLayout mParentLayout = (LinearLayout) findViewById(R.id.layoutContainer);
        Utils.setTypeFaceInViewGroup(mParentLayout, mConfig.getTFLemonRegular());

        loadData();
    }

    private File getVideoThumbnail(File videoFile) {
        String thumbName = videoFile.getName() + ".jpg";
        File thumb = new File(mConfig.getThumbDir(), thumbName);
        if (!thumb.exists()) {
            try {
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
//                Bitmap bitmap = Utils.createVideoThumb(videoFile.getAbsolutePath());
                FileOutputStream os = new FileOutputStream(thumb);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
                bitmap.recycle();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            } finally {

            }
        }
        return thumb;
    }

    /**
     * A simple class that helps adding spacing between grid items
     */
    private static class GridItemSpacingDecorator extends RecyclerView.ItemDecoration {
        private final int columnCount;
        private final int spaceInPixels;

        public GridItemSpacingDecorator(int columnCount, int spaceInPixels) {
            this.columnCount = columnCount;
            this.spaceInPixels = spaceInPixels;
        }

        public GridItemSpacingDecorator(int columnCount, int spaceInPixels, float densityMultiplier) {
            this.columnCount = columnCount;
            this.spaceInPixels = (int) (spaceInPixels * densityMultiplier);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            // We always want to add right/bottom spaceInPixels
            outRect.right = spaceInPixels;
            outRect.bottom = spaceInPixels;

            // Only add top margin for the first row
            if (parent.getChildPosition(view) < columnCount) {
                outRect.top = spaceInPixels;
            }

            // Only add left margin for the left-most items
            if (parent.getChildPosition(view) % columnCount == 0) {
                outRect.left = spaceInPixels;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_EDIT_PHOTO && resultCode == RESULT_OK) {
            mAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadData() {
        /**
         * Load media files asynchronous
         */
        new AsyncTask<Integer, Integer, Boolean>() {
            @Override
            protected void onPreExecute() {
                mPrgLoading.setVisibility(View.VISIBLE);
                mFiles.clear();
            }

            @Override
            protected Boolean doInBackground(Integer... integers) {
                UrlImageViewHelper.cleanup(GalleryActivity.this, 200);
                File pDir = mConfig.getMediaDir();
                File[] files = pDir.listFiles();
                Arrays.sort(files, Collections.reverseOrder());
                for (File file : files) {
                    String fileName = file.getAbsolutePath();
                    String fileExt = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
                    if (fileExt.equals(".mp4"))
                        getVideoThumbnail(file);
                    mFiles.add(file);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                mPrgLoading.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
            }
        }.execute();
    }
}
