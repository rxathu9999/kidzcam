package com.peerbits.kidzcamera;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.peerbits.kidzcamera.dialog.BrushDialog;
import com.peerbits.kidzcamera.dialog.EraserDialog;
import com.peerbits.kidzcamera.dialog.PaletteDialog;
import com.peerbits.kidzcamera.dialog.ProgressDialogFragment;
import com.peerbits.kidzcamera.utils.Utils;
import com.peerbits.kidzcamera.widget.DrawingPanel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class GalleryEditorActivity extends AbstractActivity implements View.OnClickListener, PaletteDialog.PaletteDialogListener {

    private static final int REQ_SAVE_PHOTO = 31;

    private DrawingPanel mDrawView;
    private LinearLayout mLayController;
    private ProgressBar mPrgLoading;
    private String mFile;
    private boolean isSaving = false;
    private boolean isShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        ImageView mBtnPaint = (ImageView) findViewById(R.id.editor_btnPaint);
        ImageView mBtnPalette = (ImageView) findViewById(R.id.editor_btnPalette);
        ImageView mBtnEraser = (ImageView) findViewById(R.id.editor_btnEraser);
        ImageView mBtnUndo = (ImageView) findViewById(R.id.editor_btnUndo);
        ImageView mBtnDelete = (ImageView) findViewById(R.id.editor_btnDelete);
        ImageView mBtnSave = (ImageView) findViewById(R.id.editor_btnSave);
        mDrawView = (DrawingPanel) findViewById(R.id.editor_drawView);
        mLayController = (LinearLayout) findViewById(R.id.editor_layoutController);
        mPrgLoading = (ProgressBar) findViewById(R.id.editor_prgLoading);

        mBtnSave.setVisibility(View.VISIBLE);
        mBtnPaint.setOnClickListener(this);
        mBtnPalette.setOnClickListener(this);
        mBtnEraser.setOnClickListener(this);
        mBtnUndo.setOnClickListener(this);
        mBtnDelete.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);

        mFile = getIntent().getStringExtra(GalleryActivity.EXTRA_FILE);

        new AsyncTask<Integer, Integer, Bitmap>() {
            @Override
            protected void onPreExecute() {
                mPrgLoading.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap doInBackground(Integer... integers) {
                Bitmap bitmap = Utils.decodeImageFile(mFile, mConfig.getScreenWidth(), mConfig.getScreenHeight());
                return Utils.convertToMutable(bitmap, mConfig.getBmpTempDir());
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                System.gc();
                mDrawView.setPhotoBitmap(bitmap);
                mPrgLoading.setVisibility(View.GONE);
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editor_btnPaint:
                if (!isShown) {
                    isShown = true;
                    mDrawView.setMode(DrawingPanel.MODE_BRUSH);
                    BrushDialog.newInstance().show(getFragmentManager());
                }
                break;
            case R.id.editor_btnPalette:
                mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down_out));
                PaletteDialog.newInstance().show(getFragmentManager());
                break;
            case R.id.editor_btnEraser:
                if (!isShown) {
                    isShown = true;
                    mDrawView.setMode(DrawingPanel.MODE_ERASER);
                    EraserDialog.newInstance().show(getFragmentManager());
                }
                break;
            case R.id.editor_btnUndo:
                mDrawView.undo();
                break;
            case R.id.editor_btnDelete:
                savePhoto();
                break;
            case R.id.editor_btnSave:
                Intent intent = new Intent(this, SavePhotoActivity.class);
                startActivityForResult(intent, REQ_SAVE_PHOTO);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode && requestCode == REQ_SAVE_PHOTO && !isSaving && !isShown) {
            isSaving = true;
            new AsyncTask<Integer, Integer, Boolean>() {
                ProgressDialogFragment dialog;
                @Override
                protected void onPreExecute() {
//                    mPrgLoading.setVisibility(View.VISIBLE);
                    dialog = ProgressDialogFragment.newInstance(R.string.message_please_wait, false);
                    dialog.show(getFragmentManager());
                }

                @Override
                protected Boolean doInBackground(Integer... integers) {
                    File file = new File(mFile);
                    mDrawView.saveTo(file);
                    File media1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    File media2 = new File(media1, "KidzCamera");
                    media2.mkdirs();
                    String newName = "KIDZ_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + ".jpg";
                    File f = new File(media2, newName);
                    Utils.copyFile(file.getAbsolutePath(), f.getAbsolutePath());
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    sendBroadcast(mediaScanIntent);
                    return null;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
//                    mPrgLoading.setVisibility(View.GONE);
                    Toast.makeText(GalleryEditorActivity.this, getString(R.string.message_photo_save_success), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    setResult(RESULT_OK);
                    finish();
                }
            }.execute();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPicked(int color) {
        mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_in));
        mDrawView.setColor(color);
    }

    @Override
    public void onDismissed(DialogFragment dialog) {
        if (dialog instanceof PaletteDialog)
            mLayController.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_in));
        isShown = false;
    }

    private void savePhoto() {
        if (isSaving || isShown)
            return;
        else
            isSaving = true;
        new AsyncTask<Integer, Integer, Boolean>() {
            ProgressDialogFragment dialog;
            @Override
            protected void onPreExecute() {
//                mPrgLoading.setVisibility(View.VISIBLE);
                dialog = ProgressDialogFragment.newInstance(R.string.message_please_wait, false);
                dialog.show(getFragmentManager());
            }

            @Override
            protected Boolean doInBackground(Integer... integers) {
                mDrawView.saveTo(new File(mFile));
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
//                mPrgLoading.setVisibility(View.GONE);
                dialog.dismiss();
                Toast.makeText(GalleryEditorActivity.this, getString(R.string.message_photo_save_success), Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
        }.execute();
    }
}
