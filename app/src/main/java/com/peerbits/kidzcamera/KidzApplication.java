package com.peerbits.kidzcamera;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.peerbits.kidzcamera.controller.GalleryCleanupReceiver;

/**
 * Created by Công on 6/2/2015.
 */
public class KidzApplication extends Application {


    public void scheduleAlarm(int sec) {
        Intent intent = new Intent(this, GalleryCleanupReceiver.class);
        intent.setAction(GalleryCleanupReceiver.ACTION_CLEAN);

        PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC, System.currentTimeMillis() + (1000 * sec), pi);
    }

    public void cancelAlarm() {
        Intent intent = new Intent(this, GalleryCleanupReceiver.class);
        intent.setAction(GalleryCleanupReceiver.ACTION_CLEAN);

        PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.cancel(pi);
    }

}
