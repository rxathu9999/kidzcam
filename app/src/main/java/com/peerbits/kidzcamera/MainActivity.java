package com.peerbits.kidzcamera;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.controller.SlidingMenuManager;
import com.peerbits.kidzcamera.fragment.PhotoCaptureFragment;
import com.peerbits.kidzcamera.fragment.VideoRecordFragment;
import com.peerbits.kidzcamera.utils.Utils;


public class MainActivity extends AbstractActivity implements View.OnClickListener {

    public static final int REQ_CHANGE_PIN = 11;
    public static final int REQ_SAVE_SETTINGS = 12;

    public static final int MODE_PHOTO = 1;
    public static final int MODE_VIDEO = 2;

    private Option mOption;
    private SlidingMenuManager mMenuMgr;
    private Toolbar mToolbar;

    private String mCurrentTag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
        Log.d("ParentPin", mOption.getParentPin());

        mMenuMgr = new SlidingMenuManager(this);
        mMenuMgr.setUp();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_camera);
        mToolbar.setNavigationIcon(R.drawable.ic_settings);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMenuMgr.getMenu().showMenu();
            }
        });

        RelativeLayout mParentLayout = (RelativeLayout) findViewById(R.id.layoutContainer);

        Utils.setTypeFaceInViewGroup(mParentLayout, mConfig.getTFLemonRegular());

        if (savedInstanceState == null) {
            Fragment fragment = PhotoCaptureFragment.newInstance();
            mCurrentTag = fragment.getClass().getName();
            getFragmentManager().beginTransaction().add(R.id.main_container, fragment, mCurrentTag).commit();
        } else {
            mCurrentTag = savedInstanceState.getString(Const.ARG_ACTIVITY_CURRENT_TAG);
            Log.w("mCurrentTag", mCurrentTag);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(Const.ARG_ACTIVITY_CURRENT_TAG, mCurrentTag);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentTag = savedInstanceState.getString(Const.ARG_ACTIVITY_CURRENT_TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_gallery) {
            Intent intent = new Intent(this, GalleryActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Option getOption() {
        return mOption;
    }

    @Override
    public void onBackPressed() {
        if (mMenuMgr.getMenu().isMenuShowing()) {
            mMenuMgr.getMenu().showContent();
            return;
        }
        if (popFragment())
            return;
        super.onBackPressed();
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (addToBackStack) {
            mCurrentTag = fragment.getClass().getName();
            transaction.addToBackStack(mCurrentTag);
        }
//        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.replace(R.id.main_container, fragment, mCurrentTag + "");
        transaction.commit();
    }

    public boolean popFragment() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            return true;
        }
        return false;
    }

    public boolean popAllFragment() {
        return getFragmentManager().getBackStackEntryCount() > 0 && getFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void changeCameraMode(int mode) {
        Fragment fragment = null;
        switch (mode) {
            case MODE_PHOTO:
                fragment = PhotoCaptureFragment.newInstance();
                break;
            case MODE_VIDEO:
                fragment = VideoRecordFragment.newInstance();
                break;
            default:
                break;
        }
        if (fragment != null) {
            popAllFragment();
            replaceFragment(fragment, false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (REQ_CHANGE_PIN == requestCode) {
                mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
            }
            if (REQ_SAVE_SETTINGS == requestCode) {
                int code = data.getIntExtra(SaveSettingActivity.SAVE_RESULT, 0);
                if (code == SaveSettingActivity.RESULT_CORRECT) {
                    mMenuMgr.saveOption();
                    Toast.makeText(this, R.string.message_setting_saved, Toast.LENGTH_SHORT).show();
                }
                if (code == SaveSettingActivity.RESULT_INCORRECT) {
                    mMenuMgr.getMenu().showContent();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }
    }

}
