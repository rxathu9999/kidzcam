package com.peerbits.kidzcamera;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.utils.Utils;


public class SavePhotoActivity extends AbstractActivity implements View.OnClickListener {

    private TextView mBtnNum[] = new TextView[10];
    private TextView mIndicator[] = new TextView[4];
    private TextView mBtnBackSpace;
    private String mCurrentPin = "";
    private Option mOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_photo);

        LinearLayout mParentLayout = (LinearLayout) findViewById(R.id.layoutContainer);
        mBtnNum[0] = (TextView) findViewById(R.id.save_btnNum0);
        mBtnNum[1] = (TextView) findViewById(R.id.save_btnNum1);
        mBtnNum[2] = (TextView) findViewById(R.id.save_btnNum2);
        mBtnNum[3] = (TextView) findViewById(R.id.save_btnNum3);
        mBtnNum[4] = (TextView) findViewById(R.id.save_btnNum4);
        mBtnNum[5] = (TextView) findViewById(R.id.save_btnNum5);
        mBtnNum[6] = (TextView) findViewById(R.id.save_btnNum6);
        mBtnNum[7] = (TextView) findViewById(R.id.save_btnNum7);
        mBtnNum[8] = (TextView) findViewById(R.id.save_btnNum8);
        mBtnNum[9] = (TextView) findViewById(R.id.save_btnNum9);
        mIndicator[0] = (TextView) findViewById(R.id.save_indicator1);
        mIndicator[1] = (TextView) findViewById(R.id.save_indicator2);
        mIndicator[2] = (TextView) findViewById(R.id.save_indicator3);
        mIndicator[3] = (TextView) findViewById(R.id.save_indicator4);
        mBtnBackSpace = (TextView) findViewById(R.id.save_btnBackSpace);

        Utils.setTypeFaceInViewGroup(mParentLayout, mConfig.getTFLemonRegular());
        for (TextView v : mBtnNum) {
            v.setOnClickListener(this);
        }
        mBtnBackSpace.setOnClickListener(this);

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_btnNum0:
                addPinNumber("0");
                break;
            case R.id.save_btnNum1:
                addPinNumber("1");
                break;
            case R.id.save_btnNum2:
                addPinNumber("2");
                break;
            case R.id.save_btnNum3:
                addPinNumber("3");
                break;
            case R.id.save_btnNum4:
                addPinNumber("4");
                break;
            case R.id.save_btnNum5:
                addPinNumber("5");
                break;
            case R.id.save_btnNum6:
                addPinNumber("6");
                break;
            case R.id.save_btnNum7:
                addPinNumber("7");
                break;
            case R.id.save_btnNum8:
                addPinNumber("8");
                break;
            case R.id.save_btnNum9:
                addPinNumber("9");
                break;
            case R.id.save_btnBackSpace:
                backSpacePin();
                break;
            default:
                break;
        }
    }

    private void backSpacePin() {
        if (mCurrentPin.length() == 0)
            return;
        mCurrentPin = mCurrentPin.substring(0, mCurrentPin.length() - 1);
        mIndicator[mCurrentPin.length()].setBackgroundResource(R.drawable.bg_pin_indicator_n);
    }

    private void addPinNumber(String number) {
        mCurrentPin += number;
        mIndicator[mCurrentPin.length() - 1].setBackgroundResource(R.drawable.bg_pin_indicator_p);
        if (mCurrentPin.length() == 4) {
            if (!mCurrentPin.equals(mOption.getParentPin())) {
                mCurrentPin = "";
                for (TextView indicator : mIndicator)
                    indicator.setBackgroundResource(R.drawable.bg_pin_indicator_n);
                Toast.makeText(this, getString(R.string.message_incorrect_pin), Toast.LENGTH_SHORT).show();
            } else {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
