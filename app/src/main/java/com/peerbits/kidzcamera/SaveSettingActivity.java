package com.peerbits.kidzcamera;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.utils.Utils;


public class SaveSettingActivity extends AbstractActivity implements View.OnClickListener {

    public static final String SAVE_RESULT = "SAVE_RESULT";
    public static final int RESULT_INCORRECT = 100;
    public static final int RESULT_CORRECT = 101;

    private TextView mBtnNum[] = new TextView[10];
    private TextView mIndicator[] = new TextView[4];
    private TextView mBtnBackSpace;
    private String mCurrentPin = "";
    private Option mOption;
    private int mIncorrectCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        LinearLayout mParentLayout = (LinearLayout) findViewById(R.id.layoutContainer);
        mBtnNum[0] = (TextView) findViewById(R.id.save_btnNum0);
        mBtnNum[1] = (TextView) findViewById(R.id.save_btnNum1);
        mBtnNum[2] = (TextView) findViewById(R.id.save_btnNum2);
        mBtnNum[3] = (TextView) findViewById(R.id.save_btnNum3);
        mBtnNum[4] = (TextView) findViewById(R.id.save_btnNum4);
        mBtnNum[5] = (TextView) findViewById(R.id.save_btnNum5);
        mBtnNum[6] = (TextView) findViewById(R.id.save_btnNum6);
        mBtnNum[7] = (TextView) findViewById(R.id.save_btnNum7);
        mBtnNum[8] = (TextView) findViewById(R.id.save_btnNum8);
        mBtnNum[9] = (TextView) findViewById(R.id.save_btnNum9);
        mIndicator[0] = (TextView) findViewById(R.id.save_indicator1);
        mIndicator[1] = (TextView) findViewById(R.id.save_indicator2);
        mIndicator[2] = (TextView) findViewById(R.id.save_indicator3);
        mIndicator[3] = (TextView) findViewById(R.id.save_indicator4);
        mBtnBackSpace = (TextView) findViewById(R.id.save_btnBackSpace);

        Utils.setTypeFaceInViewGroup(mParentLayout, mConfig.getTFLemonRegular());
        for (TextView v : mBtnNum) {
            v.setOnClickListener(this);
        }
        mBtnBackSpace.setOnClickListener(this);

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_btnNum0:
                addPinNumber("0");
                break;
            case R.id.save_btnNum1:
                addPinNumber("1");
                break;
            case R.id.save_btnNum2:
                addPinNumber("2");
                break;
            case R.id.save_btnNum3:
                addPinNumber("3");
                break;
            case R.id.save_btnNum4:
                addPinNumber("4");
                break;
            case R.id.save_btnNum5:
                addPinNumber("5");
                break;
            case R.id.save_btnNum6:
                addPinNumber("6");
                break;
            case R.id.save_btnNum7:
                addPinNumber("7");
                break;
            case R.id.save_btnNum8:
                addPinNumber("8");
                break;
            case R.id.save_btnNum9:
                addPinNumber("9");
                break;
            case R.id.save_btnBackSpace:
                backSpacePin();
                break;
            default:
                break;
        }
    }

    private void backSpacePin() {
        if (mCurrentPin.length() == 0)
            return;
        mCurrentPin = mCurrentPin.substring(0, mCurrentPin.length() - 1);
        mIndicator[mCurrentPin.length()].setBackgroundResource(R.drawable.bg_pin_indicator_n);
    }

    private void addPinNumber(String number) {
        mCurrentPin += number;
        mIndicator[mCurrentPin.length() - 1].setBackgroundResource(R.drawable.bg_pin_indicator_p);
        if (mCurrentPin.length() == 4) {
            if (!mCurrentPin.equals(mOption.getParentPin())) {
                mCurrentPin = "";
                for (TextView indicator : mIndicator)
                    indicator.setBackgroundResource(R.drawable.bg_pin_indicator_n);
                Toast.makeText(this, getString(R.string.message_incorrect_pin), Toast.LENGTH_SHORT).show();
                onIncorrectPinInput();
            } else {
                Intent data = new Intent();
                data.putExtra(SAVE_RESULT, RESULT_CORRECT);
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    private void onIncorrectPinInput() {
        mIncorrectCount += 1;
        if (mIncorrectCount == 3) {
            DialogFragment dialog = new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    setCancelable(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder .setTitle(R.string.title_alert)
                            .setMessage(R.string.message_three_times_incorrect)
                            .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent data = new Intent();
                                    data.putExtra(SAVE_RESULT, RESULT_INCORRECT);
                                    setResult(RESULT_OK, data);
                                    finish();
                                }
                            });
                    return builder.create();
                }
            };
            dialog.show(getFragmentManager(), "Dialog");
        }
    }
}
