package com.peerbits.kidzcamera;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;


public class SplashActivity extends AbstractActivity {

    private Option mOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView img = (ImageView) findViewById(R.id.splash_imgSplash);
        img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_splashin));

        mOption = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
        if (mOption == null) {
            mOption = new Option();
            mPrefs.edit().putString(Const.PREF_OPTIONS, new Gson().toJson(mOption)).apply();
            ((KidzApplication) getApplicationContext()).scheduleAlarm(mOption.getCleanTime() * 3600);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    Intent intent = null;
                    if (mOption.getParentPin().length() == 0)
                        intent = new Intent(SplashActivity.this, SetPinActivity.class);
                    else
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }


}
