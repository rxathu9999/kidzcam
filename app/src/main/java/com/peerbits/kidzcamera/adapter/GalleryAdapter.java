package com.peerbits.kidzcamera.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.peerbits.kidzcamera.GalleryActivity;
import com.peerbits.kidzcamera.GalleryEditorActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.widget.SquareImageView;

import java.io.File;
import java.util.List;

/**
 * Created by Công on 6/15/2015.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private Activity mActivity;
    private List<File> mFiles;
    private File mThumbDir;

    public GalleryAdapter(Activity activity, List<File> mediaFiles, File thumnDir) {
        mActivity = activity;
        mFiles = mediaFiles;
        mThumbDir = thumnDir;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gallery, viewGroup, false);
        ViewHolder holder = new ViewHolder(view, new ViewHolderClick() {
            @Override
            public void onClick(View view, int position) {
                File media = mFiles.get(position);
                String fileName = media.getAbsolutePath();
                String fileExt = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
                if (fileExt.equals(".mp4")) {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(media), "video/*");
                    mActivity.startActivity(intent);
                } else {
//                    Intent intent = new Intent();
//                    intent.setAction(android.content.Intent.ACTION_VIEW);
//                    intent.setDataAndType(Uri.fromFile(media), "image/*");
//                    view.getContext().startActivity(intent);
                    UrlImageViewHelper.remove(Uri.fromFile(media).toString());
                    Intent intent = new Intent(view.getContext(), GalleryEditorActivity.class);
                    intent.putExtra(GalleryActivity.EXTRA_FILE, fileName);
                    mActivity.startActivityForResult(intent, GalleryActivity.REQ_EDIT_PHOTO);
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        File media = mFiles.get(i);
        String fileName = media.getAbsolutePath();
        String fileExt = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        if (fileExt.equals(".mp4"))
            UrlImageViewHelper.setUrlDrawable(viewHolder.mImage, Uri.fromFile(new File(mThumbDir, media.getName() + ".jpg")).toString());
        else
            UrlImageViewHelper.setUrlDrawable(viewHolder.mImage, Uri.fromFile(media).toString(), R.drawable.bg_photo_thumb, 0);
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private SquareImageView mImage;
        private ViewHolderClick mListener;

        public ViewHolder(View itemView, ViewHolderClick listener) {
            super(itemView);
            mListener = listener;
            mImage = (SquareImageView) itemView;
            mImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getPosition());
        }
    }

    public interface ViewHolderClick {
        void onClick(View view, int position);
    }
}
