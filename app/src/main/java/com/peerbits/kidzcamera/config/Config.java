package com.peerbits.kidzcamera.config;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;

import java.io.File;

/**
 * Created by Công on 6/2/2015.
 */
public class Config {

    private Activity mContext;

    private int				mWidth, mHeight;
    private String			mScreenType;
    private String			mVersionName	= "";
    private int				mVersionCode	= 0;
    private Typeface        mTFLemonRegular;
    private File            mTempPhoto;
    private File            mTempVideo;
    private File            mThumbDir;
    private File            mMediaDir;
    private File            mBmpTemp;

    public Config(Activity context) {
        mContext = context;

        /**
         * Get version info
         */
        try {
            PackageInfo mPInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            mVersionName = mPInfo.versionName;
            mVersionCode = mPInfo.versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        /**
         * Get screen size
         */
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mWidth = metrics.widthPixels;
        mHeight = metrics.heightPixels;
        Log.w("Screen Size", mWidth + "x" + mHeight);

        /**
         * Get screen type
         */
        switch (mContext.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                mScreenType = "LDPI";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                mScreenType = "MDPI";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                mScreenType = "HDPI";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                mScreenType = "XHDPI";
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                mScreenType = "XXHDPI";
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                mScreenType = "XXXHDPI";
                break;
            case DisplayMetrics.DENSITY_400:
                mScreenType = "400";
                break;
            case DisplayMetrics.DENSITY_TV:
                mScreenType = "TV";
                break;
            default:
                mScreenType = "DEFAULT";
                break;
        }
        Log.w("Screen Type", mScreenType);

        // App Typeface
        mTFLemonRegular = Typeface.createFromAsset(context.getAssets(), "KGPrimaryWhimsy.ttf");

        // Cache directory
        File cacheDir = mContext.getExternalCacheDir();
        if (cacheDir == null)
            cacheDir = mContext.getCacheDir();
        mTempPhoto = new File(cacheDir, "tempPhoto.jpg");
        mTempVideo = new File(cacheDir, "tempVideo.mp4");

        // Media storage directory
        File storage = mContext.getExternalFilesDir(null);
        if (storage == null)
            storage = context.getFilesDir();
        File appRoot = new File(storage, "KidzCamera");
        if (!appRoot.exists())
            appRoot.mkdirs();
        mThumbDir = new File(appRoot, "thumbs");
        if (!mThumbDir.exists())
            mThumbDir.mkdirs();
        mMediaDir = new File(appRoot, "Media");
        if (!mMediaDir.exists())
            mMediaDir.mkdirs();
        mBmpTemp = new File(appRoot, "tmp");
        if (!mBmpTemp.exists())
            mBmpTemp.mkdirs();
    }

    public int getScreenWidth() {
        return mWidth;
    }

    public int getScreenHeight() {
        return mHeight;
    }

    public String getScreenType() {
        return mScreenType;
    }

    public String getVersionName() {
        return mVersionName;
    }

    public int getVersionCode() {
        return mVersionCode;
    }

    public Typeface getTFLemonRegular() {
        return mTFLemonRegular;
    }

    public File getTempPhoto() {
        return mTempPhoto;
    }

    public File getTempVideo() {
        return mTempVideo;
    }

    public File getMediaDir() {
        return mMediaDir;
    }

    public File getThumbDir() {
        return mThumbDir;
    }

    public File getBmpTempDir() {
        return mBmpTemp;
    }
}
