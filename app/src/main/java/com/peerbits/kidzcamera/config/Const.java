package com.peerbits.kidzcamera.config;


public class Const {
    private static final String CLASS_NAME = Const.class.getName();

	public static final boolean	DEVELOPER_MODE					= true;
	public static final String	PROPERTY_REG_ID					= "registration_id";
	public static final String	PROPERTY_APP_VERSION			= "appVersion";

    // Preference definition
    public static final String MAIN_CONTEXT = CLASS_NAME + "_PREFERENCE_MAIN_CONTEXT";
    public static final String PREF_OPTIONS = CLASS_NAME + "_PREF_OPTIONS";
    public static final String PREF_SAVE_TIME = CLASS_NAME + ".PREF_SAVE_TIME";

    // Fragment tag for saved instance state
    public static final String ARG_ACTIVITY_CURRENT_TAG = CLASS_NAME + "_ARG_ACTIVITY_CURRENT_TAG";

    // Saved camera parameters
    public static final String PREF_SUPPORTED_PICTURESIZE = CLASS_NAME + "_SUPPORTED_PICTURESIZE";
    public static final String PREF_SUPPORTED_FLASHMODE = CLASS_NAME + "_SUPPORTED_FLASHMODE";

}