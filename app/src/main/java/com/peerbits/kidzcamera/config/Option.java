package com.peerbits.kidzcamera.config;

/**
 * Created by Công on 6/2/2015.
 */
public class Option {

    private String parentPin = "";
    private int photoLimit = 25;
    private int videoLimit = 30;
    private int cleanTime = 6;
    private boolean cleanEnable = true;

    public String getParentPin() {
        return parentPin;
    }

    public void setParentPin(String parentPin) {
        this.parentPin = parentPin;
    }

    public int getPhotoLimit() {
        return photoLimit;
    }

    public void setPhotoLimit(int photoLimit) {
        this.photoLimit = photoLimit;
    }

    public int getVideoLimit() {
        return videoLimit;
    }

    public void setVideoLimit(int videoLimit) {
        this.videoLimit = videoLimit;
    }

    public int getCleanTime() {
        return cleanTime;
    }

    public void setCleanTime(int cleanTime) {
        this.cleanTime = cleanTime;
    }

    public boolean isCleanEnable() {
        return cleanEnable;
    }

    public void setCleanEnable(boolean cleanEnable) {
        this.cleanEnable = cleanEnable;
    }
}
