package com.peerbits.kidzcamera.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;

import com.google.gson.Gson;
import com.peerbits.kidzcamera.KidzApplication;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;

import java.io.File;

/**
 * Created by Công on 6/15/2015.
 */
public class GalleryCleanupReceiver extends BroadcastReceiver {

    public static final String ACTION_CLEAN = "com.peerbits.kidzcamera.controller.GalleryCleanupReceiver.ACTION_CLEAN";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            SharedPreferences mPrefs = context.getSharedPreferences(Const.MAIN_CONTEXT, Context.MODE_PRIVATE);
            Option option = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
            KidzApplication app = (KidzApplication) context.getApplicationContext();
            int cleanTime = option.getCleanTime() * 3600;
            if (cleanTime > 0)
                app.scheduleAlarm(cleanTime);
        }

        if (action.equals(ACTION_CLEAN)) {
            File storage = Environment.getExternalStorageDirectory();
            if (storage == null)
                storage = context.getFilesDir();
            File appRoot = new File(storage, "KidzCamera");
            if (!appRoot.exists())
                appRoot.mkdirs();
            File mMediaDir = new File(appRoot, "Media");
            if (mMediaDir.exists() && mMediaDir.isDirectory()) {
                File[] files = mMediaDir.listFiles();
                for (File file : files)
                    file.delete();
            }

            SharedPreferences mPrefs = context.getSharedPreferences(Const.MAIN_CONTEXT, Context.MODE_PRIVATE);
            Option option = new Gson().fromJson(mPrefs.getString(Const.PREF_OPTIONS, ""), Option.class);
            KidzApplication app = (KidzApplication) context.getApplicationContext();
            int cleanTime = option.getCleanTime() * 3600;
            if (cleanTime > 0)
                app.scheduleAlarm(cleanTime);
        }
    }
}
