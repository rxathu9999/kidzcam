package com.peerbits.kidzcamera.controller;

import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.peerbits.kidzcamera.ChangePinActivity;
import com.peerbits.kidzcamera.KidzApplication;
import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.SaveSettingActivity;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.utils.Utils;

import java.io.File;

/**
 * Created by Công on 6/2/2015.
 */
public class SlidingMenuManager implements CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener, SlidingMenu.OnOpenListener {

    private MainActivity mActivity;
    private SlidingMenu mMenu;

    private TextView mTvTitle, mBtnResetPin, mTvSeekPhotoValue, mTvSeekVideoValue, mTvSeekDelEntireValue, mBtnSave;
    private TextView mTvDelMin, mTvDelMax;
    private SeekBar mSbPhoto, mSbVideo, mSbDelEntire;
    private CheckBox mCbDelEntire;
    private boolean firstLoad = true;

    public SlidingMenuManager(MainActivity activity) {
        mActivity = activity;
        mMenu = new SlidingMenu(mActivity);
        mMenu.attachToActivity(mActivity, SlidingMenu.SLIDING_WINDOW);
    }

    public void setUp() {
        mMenu.setMode(SlidingMenu.LEFT);
        mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mMenu.setShadowDrawable(R.drawable.shadow);
        mMenu.setShadowWidth(5);
        mMenu.setBehindWidth(mActivity.getConfig().getScreenWidth() * 85 / 100);
        mMenu.setBehindScrollScale(1.0f);
        mMenu.setFadeDegree(0.75f);
        mMenu.setMenu(R.layout.sliding_menu);

        mTvTitle = (TextView) mMenu.findViewById(R.id.menu_title);
        mBtnResetPin = (TextView) mMenu.findViewById(R.id.menu_btnResetPin);
        mTvSeekPhotoValue = (TextView) mMenu.findViewById(R.id.menu_tvPhotoValue);
        mTvSeekVideoValue = (TextView) mMenu.findViewById(R.id.menu_tvVideoValue);
        mTvSeekDelEntireValue = (TextView) mMenu.findViewById(R.id.menu_tvDelEntireValue);
        mSbPhoto = (SeekBar) mMenu.findViewById(R.id.menu_sbPhoto);
        mSbVideo = (SeekBar) mMenu.findViewById(R.id.menu_sbVideo);
        mSbDelEntire = (SeekBar) mMenu.findViewById(R.id.menu_sbDelEntire);
        mCbDelEntire = (CheckBox) mMenu.findViewById(R.id.menu_cbDelEntire);
        mTvDelMax = (TextView) mMenu.findViewById(R.id.menu_tvDelMax);
        mTvDelMin = (TextView) mMenu.findViewById(R.id.menu_tvDelMin);
        mBtnSave = (TextView) mMenu.findViewById(R.id.menu_btnSave);

        mTvTitle.setText(Html.fromHtml(mActivity.getString(R.string.title_menu)));
        Utils.setTypeFaceInViewGroup(mMenu, mActivity.getConfig().getTFLemonRegular());

        mMenu.setOnOpenListener(this);
        mBtnResetPin.setOnClickListener(this);
        mSbPhoto.setOnSeekBarChangeListener(this);
        mSbVideo.setOnSeekBarChangeListener(this);
        mSbDelEntire.setOnSeekBarChangeListener(this);
        mCbDelEntire.setOnCheckedChangeListener(this);
        mBtnSave.setOnClickListener(this);
    }

    public SlidingMenu getMenu() {
        return mMenu;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        TextView text = null;
        switch (seekBar.getId()) {
            case R.id.menu_sbPhoto:
                text = mTvSeekPhotoValue;
                break;
            case R.id.menu_sbVideo:
                text = mTvSeekVideoValue;
                break;
            case R.id.menu_sbDelEntire:
                text = mTvSeekDelEntireValue;
                break;
            default:
                break;
        }
        if (text == null)
            return;
        updateSeekBarHint(seekBar, text, i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (isSettingChanged())
            mBtnSave.setEnabled(true);
        else
            mBtnSave.setEnabled(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_btnResetPin:
                mActivity.startActivityForResult(new Intent(mActivity, ChangePinActivity.class), MainActivity.REQ_CHANGE_PIN);
                break;
            case R.id.menu_btnSave:
                mActivity.startActivityForResult(new Intent(mActivity, SaveSettingActivity.class), MainActivity.REQ_SAVE_SETTINGS);
                break;
            default:
                break;
        }
    }

    private void updateSeekBarHint(SeekBar seekBar, TextView textView, int i) {
        String text = "";
        switch (seekBar.getId()) {
            case R.id.menu_sbPhoto:
                text = i + "";
                break;
            case R.id.menu_sbVideo:
                text = i + "";
                break;
            case R.id.menu_sbDelEntire:
                text = i + "";
                break;
            default:
                break;
        }
        textView.setText(text);
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, mActivity.getResources().getDisplayMetrics());
        int val = (i * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax() - textView.getWidth() / 2;
        textView.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2 + px);
//        float percent = ((float) i) / (float) seekBar.getMax();
//        int width = seekBar.getWidth() - 2 * seekBar.getThumbOffset();
//        int answer = ((int) (width * percent + seekBar.getThumbOffset() - textView.getWidth() / 2));
//        textView.setX(answer);
    }

    @Override
    public void onOpen() {
        Option option = mActivity.getOption();
        mSbPhoto.setProgress(option.getPhotoLimit());
        mSbVideo.setProgress(option.getVideoLimit());
        mSbDelEntire.setProgress(option.getCleanTime());
        mCbDelEntire.setChecked(option.isCleanEnable());

        if (firstLoad) {
            updateSeekBarHint(mSbDelEntire, mTvSeekDelEntireValue, mSbDelEntire.getProgress());
            updateSeekBarHint(mSbPhoto, mTvSeekPhotoValue, mSbPhoto.getProgress());
            updateSeekBarHint(mSbVideo, mTvSeekVideoValue, mSbVideo.getProgress());
            mSbDelEntire.setEnabled(option.isCleanEnable());
            if (option.isCleanEnable()) {
                mTvDelMin.setTextColor(Color.BLACK);
                mTvDelMax.setTextColor(Color.BLACK);
            } else {
                mTvDelMin.setTextColor(Color.GRAY);
                mTvDelMax.setTextColor(Color.GRAY);
            }
            if (isSettingChanged())
                mBtnSave.setEnabled(true);
            else
                mBtnSave.setEnabled(false);
            firstLoad = false;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.menu_cbDelEntire:
                mSbDelEntire.setEnabled(b);
                if (b) {
                    mTvDelMin.setTextColor(Color.BLACK);
                    mTvDelMax.setTextColor(Color.BLACK);
                } else {
                    mTvDelMin.setTextColor(Color.GRAY);
                    mTvDelMax.setTextColor(Color.GRAY);
                }
                if (isSettingChanged())
                    mBtnSave.setEnabled(true);
                else
                    mBtnSave.setEnabled(false);
                break;
            default:
                break;
        }
    }

    public void saveOption() {
        Option option = mActivity.getOption();
        option.setCleanEnable(mCbDelEntire.isChecked());
        option.setCleanTime(mSbDelEntire.getProgress());
        option.setPhotoLimit(mSbPhoto.getProgress());
        option.setVideoLimit(mSbVideo.getProgress());
        mActivity.getSharedPreferences().edit()
                .putString(Const.PREF_OPTIONS, new Gson().toJson(option))
                .putLong(Const.PREF_SAVE_TIME, System.currentTimeMillis())
                .apply();
        mBtnSave.setEnabled(false);
        int cleanTime = option.getCleanTime() * 3600;
        if (option.isCleanEnable() && cleanTime > 0) {
            ((KidzApplication) mActivity.getApplicationContext()).scheduleAlarm(cleanTime);
        } else {
            ((KidzApplication) mActivity.getApplicationContext()).cancelAlarm();
        }
        File[] files = mActivity.getConfig().getMediaDir().listFiles();
        if (files.length > option.getPhotoLimit()) {
            int delNum = files.length - option.getPhotoLimit();
            for (int i=0; i<delNum; i++)
                files[i].delete();
        }
    }

    private boolean isSettingChanged() {
        if (mSbPhoto.getProgress() != mActivity.getOption().getPhotoLimit())
            return true;
        if (mSbVideo.getProgress() != mActivity.getOption().getVideoLimit())
            return true;
        if (mCbDelEntire.isChecked() != mActivity.getOption().isCleanEnable())
            return true;
        if (mCbDelEntire.isChecked() && mActivity.getOption().getCleanTime() != mSbDelEntire.getProgress())
            return true;
        return false;
    }
}
