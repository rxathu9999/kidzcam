/**
 * Sep 30, 2014 2:21:26 PM
 */
package com.peerbits.kidzcamera.dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peerbits.kidzcamera.R;


/**
 * @author Cong
 *
 */
public class BrushDialog extends DialogFragment {

    private PaletteDialog.PaletteDialogListener mListener;

    public static BrushDialog newInstance() {
        BrushDialog dialog = new BrushDialog();
        return dialog;
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NORMAL, R.style.Theme_Transparent);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (PaletteDialog.PaletteDialogListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().getAttributes().windowAnimations = R.anim.scale_in;
        getDialog().setCancelable(false);
        View view = inflater.inflate(R.layout.dialog_brush, container, false);
        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null)
            mListener.onDismissed(this);
    }

    @Deprecated
	@Override
	public void show(FragmentManager manager, String tag) {
		super.show(manager, tag);
	}
	
	public void show(FragmentManager manager) {
		super.show(manager, "Dialog");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isAdded())
                    dismiss();
            }
        }, 1000);
	}
}
