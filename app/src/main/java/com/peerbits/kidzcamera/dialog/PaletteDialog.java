/**
 * Sep 30, 2014 2:21:26 PM
 */
package com.peerbits.kidzcamera.dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.peerbits.kidzcamera.R;


/**
 * @author Cong
 *
 */
public class PaletteDialog extends DialogFragment implements View.OnClickListener {

	private PaletteDialogListener mListener;
    private ImageView mBtnYellow, mBtnOrange, mBtnRed, mBtnGreen, mBtnBlue;

    public static PaletteDialog newInstance() {
        PaletteDialog dialog = new PaletteDialog();
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (PaletteDialogListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NORMAL, R.style.Theme_Transparent);
	}

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().getAttributes().windowAnimations = R.anim.scale_in;
        getDialog().setCancelable(false);
        View view = inflater.inflate(R.layout.dialog_palette, container, false);

        mBtnYellow = (ImageView) view.findViewById(R.id.dialogPalette_btnYellow);
        mBtnOrange = (ImageView) view.findViewById(R.id.dialogPalette_btnOrange);
        mBtnRed = (ImageView) view.findViewById(R.id.dialogPalette_btnRed);
        mBtnGreen = (ImageView) view.findViewById(R.id.dialogPalette_btnGreen);
        mBtnBlue = (ImageView) view.findViewById(R.id.dialogPalette_btnBlue);

        mBtnYellow.setOnClickListener(this);
        mBtnOrange.setOnClickListener(this);
        mBtnRed.setOnClickListener(this);
        mBtnGreen.setOnClickListener(this);
        mBtnBlue.setOnClickListener(this);

        return view;
    }

    @Deprecated
	@Override
	public void show(FragmentManager manager, String tag) {
		super.show(manager, tag);
	}
	
	public void show(FragmentManager manager) {
		super.show(manager, "Dialog");
	}

    @Override
    public void onClick(View view) {
        if (mListener == null)
            return;
        switch (view.getId()) {
            case R.id.dialogPalette_btnYellow:
                mListener.onPicked(Color.YELLOW);
                break;
            case R.id.dialogPalette_btnOrange:
                mListener.onPicked(Color.parseColor("#FFA500"));
                break;
            case R.id.dialogPalette_btnRed:
                mListener.onPicked(Color.RED);
                break;
            case R.id.dialogPalette_btnGreen:
                mListener.onPicked(Color.GREEN);
                break;
            case R.id.dialogPalette_btnBlue:
                mListener.onPicked(Color.BLUE);
                break;
            default:
                break;
        }
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null)
            mListener.onDismissed(this);
    }

    public interface PaletteDialogListener {
        void onPicked(int color);
        void onDismissed(DialogFragment dialog);
    }
}
