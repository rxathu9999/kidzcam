package com.peerbits.kidzcamera.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;

/**
 * Created by Công on 6/18/2015.
 */
public class ProgressDialogFragment extends DialogFragment {

    private static final String ARG_MESSAGE_RESOURCE_ID = "ARG_MESSAGE_RESOURCE_ID";
    private static final String ARG_CANCELABLE = "ARG_CANCELABLE";

    public static ProgressDialogFragment newInstance(int messId, boolean cancelable) {
        ProgressDialogFragment dialog = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_MESSAGE_RESOURCE_ID, messId);
        bundle.putBoolean(ARG_CANCELABLE, cancelable);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(getArguments().getBoolean(ARG_CANCELABLE));
        dialog.setMessage(getString(getArguments().getInt(ARG_MESSAGE_RESOURCE_ID)));
        return dialog;
    }

    @Override
    @Deprecated
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    public void show(FragmentManager manager) {
        super.show(manager, "Dialog");
    }
}
