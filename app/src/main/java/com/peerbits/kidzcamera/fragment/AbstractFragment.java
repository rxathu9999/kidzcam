/**
 * May 2, 2014 11:20:55 AM
 */
package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.peerbits.kidzcamera.AbstractActivity;

/**
 * @author Cong
 *
 */
public class AbstractFragment extends Fragment {
	
	protected AbstractActivity mActivity;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.w(getClass().getSimpleName(), "onCreate" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.w(getClass().getSimpleName(), "onActivityCreated" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity) {
		Log.w(getClass().getSimpleName(), "onAttach" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onAttach(activity);
        mActivity = (AbstractActivity) activity;
	}
	
	@Override
	public void onDestroy() {
		Log.w(getClass().getSimpleName(), "onDestroy" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onDestroy();
	}
	
	@Override
	public void onDestroyView() {
		Log.w(getClass().getSimpleName(), "onDestroyView" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onDestroyView();
	}
	
	@Override
	public void onDetach() {
		Log.w(getClass().getSimpleName(), "onDetach" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onDetach();
        mActivity = null;
	}
	
	@Override
	public void onPause() {
		Log.w(getClass().getSimpleName(), "onPause" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onPause();
	}
	
	@Override
	public void onResume() {
		Log.w(getClass().getSimpleName(), "onResume" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onResume();
	}
	
	@Override
	public void onStart() {
		Log.w(getClass().getSimpleName(), "onStart" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onStart();
	}
	
	@Override
	public void onStop() {
		Log.w(getClass().getSimpleName(), "onStop" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onStop();
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.w(getClass().getSimpleName(), "onViewCreated" + " / " + System.identityHashCode(this) + " / " + getTag());
	}
	
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		Log.w(getClass().getSimpleName(), "onViewStateRestored" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onViewStateRestored(savedInstanceState);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.w(getClass().getSimpleName(), "onActivityResult" + " / " + System.identityHashCode(this) + " / " + getTag());
		super.onActivityResult(requestCode, resultCode, data);
	}
}
