package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.peerbits.kidzcamera.EditorActivity;
import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Công on 6/3/2015.
 */
@SuppressWarnings("deprecation")
public class CWPhotoCaptureFragment extends CameraFragment implements View.OnClickListener, Camera.ShutterCallback {

    private MainActivity    mActivity;
    private FrameLayout     mCameraView;

    private ImageButton mBtnCamera, mBtnVideo;
    private ImageView mImgThumb;

    public static CWPhotoCaptureFragment newInstance() {
        CWPhotoCaptureFragment fragment = new CWPhotoCaptureFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleCameraHost.Builder builder = new SimpleCameraHost.Builder(new MyCameraHost(mActivity));
        setHost(builder.useFullBleedPreview(true).build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        View cameraView = super.onCreateView(inflater, container, savedInstanceState);

        mCameraView = (FrameLayout) view.findViewById(R.id.camera_container);
        mBtnCamera = (ImageButton) view.findViewById(R.id.camera_btnCamera);
        mBtnVideo = (ImageButton) view.findViewById(R.id.camera_btnVideo);
        mImgThumb = (ImageView) view.findViewById(R.id.camera_imgThumb);

        mBtnVideo.setOnClickListener(this);
        mBtnCamera.setOnClickListener(this);
        mImgThumb.setOnClickListener(this);

        mCameraView.addView(cameraView);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.camera_btnCamera:
                takePicture();
                break;
            case R.id.camera_btnVideo:
                mActivity.changeCameraMode(MainActivity.MODE_VIDEO);
                break;
            case R.id.camera_imgThumb:
                if (mActivity.getConfig().getTempPhoto().exists()) {
                    Intent intent = new Intent(getActivity(), EditorActivity.class);
                    startActivity(intent);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onShutter() {
        AudioManager mgr = (AudioManager) mActivity.getSystemService(Context.AUDIO_SERVICE);
        mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
    }

    private class MyCameraHost extends SimpleCameraHost {

        public MyCameraHost(Context _ctxt) {
            super(_ctxt);
        }

        @Override
        public RecordingHint getRecordingHint() {
            return RecordingHint.STILL_ONLY;
        }

        @Override
        public boolean useFrontFacingCamera() {
            return false;
        }

        @Override
        public boolean useSingleShotMode() {
            return false;
        }

        @Override
        public void saveImage(PictureTransaction xact, byte[] image) {
            final File photo = mActivity.getConfig().getTempPhoto();
            try {
                FileOutputStream fos = new FileOutputStream(photo);
                fos.write(image);
                fos.close();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.displayImageFromFile(mImgThumb, photo.getAbsolutePath());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Start camera preview again to capture more photo
//            camera.startPreview();
        }

        @Override
        public void onCameraFail(CameraHost.FailureReason reason) {
            super.onCameraFail(reason);

            Toast.makeText(getActivity(),
                    "Sorry, but you cannot use the camera now!",
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
            return(super.adjustPreviewParameters(parameters));
        }
    }
}