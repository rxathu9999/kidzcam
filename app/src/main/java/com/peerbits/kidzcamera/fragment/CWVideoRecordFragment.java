package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by Công on 6/3/2015.
 */
@SuppressWarnings("deprecation")
public class CWVideoRecordFragment extends CameraFragment implements View.OnClickListener, Camera.ShutterCallback {

    private MainActivity    mActivity;
    private FrameLayout     mCameraView;

    private ImageButton mBtnCamera, mBtnVideo, mBtnPause;
    private ImageView mImgThumb;

    public static CWVideoRecordFragment newInstance() {
        CWVideoRecordFragment fragment = new CWVideoRecordFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleCameraHost.Builder builder = new SimpleCameraHost.Builder(new MyCameraHost(mActivity));
        setHost(builder.useFullBleedPreview(true).build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        View cameraView = super.onCreateView(inflater, container, savedInstanceState);

        mCameraView = (FrameLayout) view.findViewById(R.id.video_container);
        mBtnCamera = (ImageButton) view.findViewById(R.id.video_btnCamera);
        mBtnVideo = (ImageButton) view.findViewById(R.id.video_btnRecording);
        mImgThumb = (ImageView) view.findViewById(R.id.video_imgThumb);
        mBtnPause = (ImageButton) view.findViewById(R.id.video_btnPause);

        mBtnVideo.setOnClickListener(this);
        mBtnCamera.setOnClickListener(this);
        mImgThumb.setOnClickListener(this);

        lockToLandscape(true);
        mCameraView.addView(cameraView);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_btnCamera:
                mActivity.changeCameraMode(MainActivity.MODE_PHOTO);
                break;
            case R.id.video_btnRecording:
                if (isRecording())
                    try {
                        stopRecording();
                        mBtnVideo.startAnimation(createButtonAnimation(mBtnVideo, R.drawable.ic_video));
                        mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_pause));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else
                    try {
                        record();
                        mBtnVideo.startAnimation(createButtonAnimation(mBtnVideo, R.drawable.ic_pause));
                        mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_video));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                break;
            case R.id.video_btnPause:
                if (isRecording())
                    try {
                        stopRecording();
                        mBtnVideo.startAnimation(createButtonAnimation(mBtnVideo, R.drawable.ic_video));
                        mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_pause));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else
                    try {
                        record();
                        mBtnVideo.startAnimation(createButtonAnimation(mBtnVideo, R.drawable.ic_pause));
                        mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_video));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                break;
            case R.id.camera_imgThumb:
                File media = mActivity.getConfig().getTempVideo();
                if (media.exists()) {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(media), "video/*");
                    view.getContext().startActivity(intent);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onShutter() {
        AudioManager mgr = (AudioManager) mActivity.getSystemService(Context.AUDIO_SERVICE);
        mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
    }

    private class MyCameraHost extends SimpleCameraHost {

        public MyCameraHost(Context _ctxt) {
            super(_ctxt);
        }

        @Override
        public RecordingHint getRecordingHint() {
            return RecordingHint.VIDEO_ONLY;
        }

        @Override
        protected File getVideoPath() {
            return mActivity.getConfig().getTempVideo();
        }

        @Override
        public void configureRecorderProfile(int cameraId, MediaRecorder recorder) {
            super.configureRecorderProfile(cameraId, recorder);
        }

        @Override
        public boolean useFrontFacingCamera() {
            return false;
        }

        @Override
        public boolean useSingleShotMode() {
            return true;
        }

        @Override
        public void configureRecorderOutput(int cameraId, MediaRecorder recorder) {
            recorder.setMaxDuration(mActivity.getOption().getVideoLimit() * 1000);
            super.configureRecorderOutput(cameraId, recorder);
        }

        @Override
        public void configureRecorderAudio(int cameraId, MediaRecorder recorder) {
            super.configureRecorderAudio(cameraId, recorder);
        }

        @Override
        public void onCameraFail(FailureReason reason) {
            super.onCameraFail(reason);

            Toast.makeText(getActivity(),
                    "Sorry, but you cannot use the camera now!",
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
            return(super.adjustPreviewParameters(parameters));
        }
    }

    private Animation createButtonAnimation(final ImageButton button, final int resId) {
        Animation anim = AnimationUtils.loadAnimation(mActivity, R.anim.scale_btn_camera);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                button.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                button.setImageResource(resId);
            }
        });
        return anim;
    }
}