package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;

/**
 * Created by Công on 6/16/2015.
 */
public class MainFragment extends AbstractFragment implements View.OnClickListener {

    private MainActivity mActivity;
    private ImageButton mBtnCamera, mBtnVideo;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mBtnCamera = (ImageButton) view.findViewById(R.id.main_btnCamera);
        mBtnVideo = (ImageButton) view.findViewById(R.id.main_btnVideo);

        mBtnCamera.setOnClickListener(this);
        mBtnVideo.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_btnCamera:
                mActivity.changeCameraMode(MainActivity.MODE_PHOTO);
                break;
            case R.id.main_btnVideo:
                mActivity.changeCameraMode(MainActivity.MODE_VIDEO);
                break;
            default:
                break;
        }
    }
}
