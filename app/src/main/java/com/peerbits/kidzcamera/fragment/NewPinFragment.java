package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peerbits.kidzcamera.ChangePinActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.utils.Utils;

/**
 * Created by Công on 6/18/2015.
 */
public class NewPinFragment extends AbstractFragment implements View.OnClickListener {

    private ChangePinActivity mActivity;
    private TextView mBtnNum[] = new TextView[10];
    private TextView mIndicator[] = new TextView[4];
    private TextView mBtnBackSpace;
    private String mCurrentPin = "";

    public static NewPinFragment newInstance() {
        return new NewPinFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pin, container, false);

        LinearLayout mParentLayout = (LinearLayout) view.findViewById(R.id.layoutContainer);
        mBtnNum[0] = (TextView) view.findViewById(R.id.changePin_btnNum0);
        mBtnNum[1] = (TextView) view.findViewById(R.id.changePin_btnNum1);
        mBtnNum[2] = (TextView) view.findViewById(R.id.changePin_btnNum2);
        mBtnNum[3] = (TextView) view.findViewById(R.id.changePin_btnNum3);
        mBtnNum[4] = (TextView) view.findViewById(R.id.changePin_btnNum4);
        mBtnNum[5] = (TextView) view.findViewById(R.id.changePin_btnNum5);
        mBtnNum[6] = (TextView) view.findViewById(R.id.changePin_btnNum6);
        mBtnNum[7] = (TextView) view.findViewById(R.id.changePin_btnNum7);
        mBtnNum[8] = (TextView) view.findViewById(R.id.changePin_btnNum8);
        mBtnNum[9] = (TextView) view.findViewById(R.id.changePin_btnNum9);
        mIndicator[0] = (TextView) view.findViewById(R.id.changePin_indicator1);
        mIndicator[1] = (TextView) view.findViewById(R.id.changePin_indicator2);
        mIndicator[2] = (TextView) view.findViewById(R.id.changePin_indicator3);
        mIndicator[3] = (TextView) view.findViewById(R.id.changePin_indicator4);
        mBtnBackSpace = (TextView) view.findViewById(R.id.changePin_btnBackSpace);

        view.findViewById(R.id.changePin_tvPinQuestion).setVisibility(View.GONE);
        view.findViewById(R.id.changePin_layoutCaptcha).setVisibility(View.GONE);

        TextView title = (TextView) view.findViewById(R.id.changePin_tvTitle);
        title.setText(R.string.title_new_pin);

        Utils.setTypeFaceInViewGroup(mParentLayout, mActivity.getConfig().getTFLemonRegular());
        for (TextView v : mBtnNum) {
            v.setOnClickListener(this);
        }
        mBtnBackSpace.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ChangePinActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changePin_btnNum0:
                addPinNumber("0");
                break;
            case R.id.changePin_btnNum1:
                addPinNumber("1");
                break;
            case R.id.changePin_btnNum2:
                addPinNumber("2");
                break;
            case R.id.changePin_btnNum3:
                addPinNumber("3");
                break;
            case R.id.changePin_btnNum4:
                addPinNumber("4");
                break;
            case R.id.changePin_btnNum5:
                addPinNumber("5");
                break;
            case R.id.changePin_btnNum6:
                addPinNumber("6");
                break;
            case R.id.changePin_btnNum7:
                addPinNumber("7");
                break;
            case R.id.changePin_btnNum8:
                addPinNumber("8");
                break;
            case R.id.changePin_btnNum9:
                addPinNumber("9");
                break;
            case R.id.changePin_btnBackSpace:
                backSpacePin();
                break;
            default:
                break;
        }
    }

    private void backSpacePin() {
        if (mCurrentPin.length() == 0)
            return;
        mCurrentPin = mCurrentPin.substring(0, mCurrentPin.length() - 1);
        mIndicator[mCurrentPin.length()].setBackgroundResource(R.drawable.bg_pin_indicator_n);
    }

    private void addPinNumber(String number) {
        mCurrentPin += number;
        mIndicator[mCurrentPin.length() - 1].setBackgroundResource(R.drawable.bg_pin_indicator_p);
        if (mCurrentPin.length() == 4) {
            mActivity.saveNewPin(mCurrentPin);
        }
    }
}
