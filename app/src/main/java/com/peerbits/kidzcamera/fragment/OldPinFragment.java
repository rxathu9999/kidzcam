package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.peerbits.kidzcamera.ChangePinActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.utils.Utils;

import java.util.Locale;

/**
 * Created by Công on 6/18/2015.
 */
public class OldPinFragment extends AbstractFragment implements View.OnClickListener, TextWatcher {

    private static final int PIN1 = 6, PIN2 = 7, ANS = 42;

    private ChangePinActivity mActivity;
    private TextView mBtnNum[] = new TextView[10];
    private TextView mIndicator[] = new TextView[4];
    private TextView mBtnBackSpace;
    private String mCurrentPin = "";
    private TextView mTvResetPin, mTvQuestion;
    private EditText mEtAnswer;

    public static OldPinFragment newInstance() {
        return new OldPinFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pin, container, false);

        LinearLayout mParentLayout = (LinearLayout) view.findViewById(R.id.layoutContainer);
        mBtnNum[0] = (TextView) view.findViewById(R.id.changePin_btnNum0);
        mBtnNum[1] = (TextView) view.findViewById(R.id.changePin_btnNum1);
        mBtnNum[2] = (TextView) view.findViewById(R.id.changePin_btnNum2);
        mBtnNum[3] = (TextView) view.findViewById(R.id.changePin_btnNum3);
        mBtnNum[4] = (TextView) view.findViewById(R.id.changePin_btnNum4);
        mBtnNum[5] = (TextView) view.findViewById(R.id.changePin_btnNum5);
        mBtnNum[6] = (TextView) view.findViewById(R.id.changePin_btnNum6);
        mBtnNum[7] = (TextView) view.findViewById(R.id.changePin_btnNum7);
        mBtnNum[8] = (TextView) view.findViewById(R.id.changePin_btnNum8);
        mBtnNum[9] = (TextView) view.findViewById(R.id.changePin_btnNum9);
        mIndicator[0] = (TextView) view.findViewById(R.id.changePin_indicator1);
        mIndicator[1] = (TextView) view.findViewById(R.id.changePin_indicator2);
        mIndicator[2] = (TextView) view.findViewById(R.id.changePin_indicator3);
        mIndicator[3] = (TextView) view.findViewById(R.id.changePin_indicator4);
        mTvQuestion = (TextView) view.findViewById(R.id.changePin_tvPinQuestion);
        mTvResetPin = (TextView) view.findViewById(R.id.changePin_tvForgetPin);
        mEtAnswer = (EditText) view.findViewById(R.id.changePin_etPinAnswer);
        mBtnBackSpace = (TextView) view.findViewById(R.id.changePin_btnBackSpace);

        mTvResetPin.setText(Html.fromHtml(getString(R.string.text_not_know_parent_pin)));
        mTvQuestion.setText(String.format(Locale.US, getString(R.string.text_captcha_question), PIN1, PIN2));

        mCurrentPin = "";
        resetPin();

        Utils.setTypeFaceInViewGroup(mParentLayout, mActivity.getConfig().getTFLemonRegular());
        for (TextView v : mBtnNum) {
            v.setOnClickListener(this);
        }
        mBtnBackSpace.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ChangePinActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mEtAnswer.setText("");
        mEtAnswer.addTextChangedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEtAnswer.removeTextChangedListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changePin_btnNum0:
                addPinNumber("0");
                break;
            case R.id.changePin_btnNum1:
                addPinNumber("1");
                break;
            case R.id.changePin_btnNum2:
                addPinNumber("2");
                break;
            case R.id.changePin_btnNum3:
                addPinNumber("3");
                break;
            case R.id.changePin_btnNum4:
                addPinNumber("4");
                break;
            case R.id.changePin_btnNum5:
                addPinNumber("5");
                break;
            case R.id.changePin_btnNum6:
                addPinNumber("6");
                break;
            case R.id.changePin_btnNum7:
                addPinNumber("7");
                break;
            case R.id.changePin_btnNum8:
                addPinNumber("8");
                break;
            case R.id.changePin_btnNum9:
                addPinNumber("9");
                break;
            case R.id.changePin_btnBackSpace:
                backSpacePin();
                break;
            default:
                break;
        }
        InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtAnswer.getWindowToken(), 0);
    }

    private void backSpacePin() {
        if (mCurrentPin.length() == 0)
            return;
        mCurrentPin = mCurrentPin.substring(0, mCurrentPin.length() - 1);
        mIndicator[mCurrentPin.length()].setBackgroundResource(R.drawable.bg_pin_indicator_n);
    }

    private void addPinNumber(String number) {
        mCurrentPin += number;
        mIndicator[mCurrentPin.length() - 1].setBackgroundResource(R.drawable.bg_pin_indicator_p);
        if (mCurrentPin.length() == 4) {
            if (mActivity.getOption().getParentPin().equals(mCurrentPin)) {
                mActivity.replaceFragment(NewPinFragment.newInstance(), true);
            } else {
                Toast.makeText(mActivity, R.string.message_invalid_old_pin, Toast.LENGTH_SHORT).show();
                resetPin();
            }
        }
    }

    private void resetPin() {
        mCurrentPin = "";
        for (TextView indicator : mIndicator) {
            indicator.setBackgroundResource(R.drawable.bg_pin_indicator_n);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String str = editable.toString();
        if (str.length() != 2)
            return;
        try {
            int answer = Integer.parseInt(str);
            if (answer == ANS) {
                InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mEtAnswer.getWindowToken(), 0);
                mActivity.replaceFragment(NewPinFragment.newInstance(), true);
            } else {
                mEtAnswer.setText("");
                Toast.makeText(mActivity, R.string.message_invalid_captcha, Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException e) {
        }
    }
}
