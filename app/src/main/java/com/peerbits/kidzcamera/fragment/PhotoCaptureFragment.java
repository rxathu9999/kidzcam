package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbits.kidzcamera.GalleryActivity;
import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.utils.Utils;
import com.peerbits.kidzcamera.widget.CameraSurface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Công on 6/3/2015.
 */
@SuppressWarnings("deprecation")
public class PhotoCaptureFragment extends AbstractFragment implements View.OnClickListener, Camera.PictureCallback, Camera.ShutterCallback {

    private MainActivity    mActivity;
    private Camera			mCamera;
    private CameraSurface   mPreview;
    private FrameLayout     mCameraView, mThumbLayout;
    private List<Camera.Size> mImageSizes = new ArrayList<>();
    private List<String> mFlashs = new ArrayList<>();

    private ImageButton mBtnCamera, mBtnVideo;
    private ImageView mImgThumb;

    private int mWidth, mHeight;

    public static PhotoCaptureFragment newInstance() {
        PhotoCaptureFragment fragment = new PhotoCaptureFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);

        mCameraView = (FrameLayout) view.findViewById(R.id.camera_container);
        mBtnCamera = (ImageButton) view.findViewById(R.id.camera_btnCamera);
        mBtnVideo = (ImageButton) view.findViewById(R.id.camera_btnVideo);
        mImgThumb = (ImageView) view.findViewById(R.id.camera_imgThumb);
        mThumbLayout = (FrameLayout) view.findViewById(R.id.camera_layoutThumb);

        mBtnVideo.setOnClickListener(this);
        mBtnCamera.setOnClickListener(this);
        mImgThumb.setOnClickListener(this);

        mThumbLayout.setVisibility(View.INVISIBLE);

        File file = mActivity.getConfig().getTempPhoto();
        if (file.exists())
            file.delete();

        mCameraView.setKeepScreenOn(true);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCameraAndPreview();
    }

    @Override
    public void onResume() {
        super.onResume();
        safeCameraOpenInView(mCameraView);
    }

    private boolean safeCameraOpenInView(ViewGroup view) {
        releaseCameraAndPreview();
        // find back camera
        int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        for (int i = 0; i<Camera.getNumberOfCameras(); i++) {
            cameraId = i;
            if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                break;
        }

        mCamera = getCameraInstance(cameraId);
        if (mCamera == null)
            return false;

        mPreview = new CameraSurface(mActivity, mCamera, cameraId);
        view.addView(mPreview);
//
//        mPreview.startCameraPreview();

        return true;
    }

    /**
     * Clear any existing preview / camera.
     */
    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        if (mPreview != null) {
            mPreview.destroyDrawingCache();
            mCameraView.removeView(mPreview);
            mPreview = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            SharedPreferences mPrefs = mActivity.getSharedPreferences();
            ActivityManager am = (ActivityManager) mActivity.getSystemService(Activity.ACTIVITY_SERVICE);
            int MaxMemory = am.getMemoryClass() / 4;

            c = Camera.open(cameraId);
            Camera.Parameters parameters = c.getParameters();

            if (mPrefs.getString(Const.PREF_SUPPORTED_PICTURESIZE, "").length() == 0) {
                mImageSizes = new ArrayList<>();
                List<Camera.Size> ls = parameters.getSupportedPictureSizes();

                for (int i=0; i<ls.size(); i++) {
                    Camera.Size s = ls.get(i);
                    if (s.width * s.height <= 1920 * 1080 && s.width * s.height * 4 <= MaxMemory * 1000000)
                        mImageSizes.add(s);
                }
                mPrefs.edit().putString(Const.PREF_SUPPORTED_PICTURESIZE, new Gson().toJson(mImageSizes)).apply();
            } else
                mImageSizes = new Gson().fromJson(mPrefs.getString(Const.PREF_SUPPORTED_PICTURESIZE, ""), new TypeToken<List<Camera.Size>>(){}.getType());

            if (mPrefs.getString(Const.PREF_SUPPORTED_FLASHMODE, "").length() == 0) {
                mFlashs = parameters.getSupportedFlashModes();
                if (mFlashs != null && mFlashs.size() > 0)
                    mPrefs.edit().putString(Const.PREF_SUPPORTED_FLASHMODE, new Gson().toJson(mFlashs)).apply();
            } else
                mFlashs = new Gson().fromJson(mPrefs.getString(Const.PREF_SUPPORTED_FLASHMODE, ""), new TypeToken<List<String>>(){}.getType());

            if (mFlashs != null && mFlashs.size() > 0) {
                if (mFlashs.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                }
            }
            // Auto focus mode
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes != null && focusModes.size() > 0) {
                if (Build.VERSION.SDK_INT >= 14 && focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            Camera.Size s = mImageSizes.get(0);
            mWidth = s.width; mHeight = s.height;

            parameters.setPictureSize(mWidth, mHeight);
            c.setParameters(parameters);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.camera_btnCamera:
                mBtnCamera.setEnabled(false);
                mCamera.takePicture(this, null, this);
                break;
            case R.id.camera_btnVideo:
                mActivity.changeCameraMode(MainActivity.MODE_VIDEO);
                break;
            case R.id.camera_imgThumb:
                Intent intent = new Intent(getActivity(), GalleryActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
//        File photo = mActivity.getConfig().getTempPhoto();
        Option option = mActivity.getOption();
        if (option.getPhotoLimit() == 0) {
            mThumbLayout.setVisibility(View.INVISIBLE);
        } else {
            if (getStorageCount() >= option.getPhotoLimit()) {
                File[] files = mActivity.getConfig().getMediaDir().listFiles();
                Arrays.sort(files);
                if (files.length > 0)
                    files[0].delete();

            }
            String s = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(new Date()) + ".jpg";
            File photo = new File(mActivity.getConfig().getMediaDir(), s);
            try {
                FileOutputStream fos = new FileOutputStream(photo);
                fos.write(bytes);
                fos.close();
                rotateImage(photo.getAbsolutePath());
                Utils.displayImageFromFile(mImgThumb, photo.getAbsolutePath());
                mThumbLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Start camera preview again to capture more photo
            camera.startPreview();
            mBtnCamera.setEnabled(true);
        }
    }

    private int getStorageCount() {
        File[] files = mActivity.getConfig().getMediaDir().listFiles();
        return files.length;
    }

    @Override
    public void onShutter() {
        AudioManager mgr = (AudioManager) mActivity.getSystemService(Context.AUDIO_SERVICE);
        mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
    }

    private void rotateImage(String filePath) {
        int rotate = Utils.getImageRotation(filePath);
        if (rotate > 0) {
            try {
                Bitmap bm = BitmapFactory.decodeFile(filePath);
                Matrix mx = new Matrix();
                mx.postRotate(rotate);
                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), mx, true);
                FileOutputStream os = new FileOutputStream(new File(filePath));
                bm.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}