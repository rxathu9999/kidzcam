package com.peerbits.kidzcamera.fragment;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbits.kidzcamera.MainActivity;
import com.peerbits.kidzcamera.R;
import com.peerbits.kidzcamera.config.Const;
import com.peerbits.kidzcamera.config.Option;
import com.peerbits.kidzcamera.dialog.ProgressDialogFragment;
import com.peerbits.kidzcamera.utils.Utils;
import com.peerbits.kidzcamera.widget.CameraSurface;
import com.peerbits.kidzcamera.widget.MagicTextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Công on 6/12/2015.
 */
public class VideoRecordFragment extends AbstractFragment implements View.OnClickListener, MediaRecorder.OnInfoListener {

    private MainActivity    mActivity;
    private Camera			mCamera;
    private CameraSurface   mPreview;
    private FrameLayout     mCameraView, mThumbLayout;
    private MediaRecorder   mediaRecorder;
    private MagicTextView   mTvElapsed;

    private Timer mRecordTimer;
    private int mElapsedTime;
    private ElapsedCounter mElapsedCounter;

    private ImageButton mBtnCamera, mBtnRecord, mBtnPause;
    private ImageView mImgThumb;

    private boolean mRecording = false;
    private int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

    private List<Camera.Size> mImageSizes = new ArrayList<>();

    private int mWidth, mHeight;
    private boolean isSaving = false;

    public static VideoRecordFragment newInstance() {
        VideoRecordFragment fragment = new VideoRecordFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        mCameraView = (FrameLayout) view.findViewById(R.id.video_container);
        mBtnCamera = (ImageButton) view.findViewById(R.id.video_btnCamera);
        mBtnRecord = (ImageButton) view.findViewById(R.id.video_btnRecording);
        mBtnPause = (ImageButton) view.findViewById(R.id.video_btnPause);
        mImgThumb = (ImageView) view.findViewById(R.id.video_imgThumb);
        mTvElapsed = (MagicTextView) view.findViewById(R.id.video_tvElapsedTime);
        mThumbLayout = (FrameLayout) view.findViewById(R.id.video_layoutThumb);

        mBtnRecord.setOnClickListener(this);
        mBtnCamera.setOnClickListener(this);
        mBtnPause.setOnClickListener(this);
        mImgThumb.setOnClickListener(this);

        mThumbLayout.setVisibility(View.INVISIBLE);
        mTvElapsed.setTypeface(mActivity.getConfig().getTFLemonRegular());
        mCameraView.setKeepScreenOn(true);

        File file = mActivity.getConfig().getTempVideo();
        if (file.exists())
            file.delete();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        stopRecording();
        releaseCameraAndPreview();
    }

    @Override
    public void onResume() {
        super.onResume();
        safeCameraOpenInView(mCameraView);
    }

    private boolean safeCameraOpenInView(ViewGroup view) {
        releaseCameraAndPreview();
        // find back camera
        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        for (int i = 0; i<Camera.getNumberOfCameras(); i++) {
            cameraId = i;
            if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                break;
        }

        mCamera = getCameraInstance(cameraId);
        if (mCamera == null)
            return false;
        mPreview = new CameraSurface(mActivity, mCamera, cameraId);
        view.addView(mPreview);
//        mPreview.startCameraPreview();

        return true;
    }

    /**
     * Clear any existing preview / camera.
     */
    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        if (mPreview != null) {
            mPreview.destroyDrawingCache();
            mCameraView.removeView(mPreview);
            mPreview = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Safe method for getting a camera instance.
     */
    public Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            SharedPreferences mPrefs = mActivity.getSharedPreferences();
            ActivityManager am = (ActivityManager) mActivity.getSystemService(Activity.ACTIVITY_SERVICE);
            int MaxMemory = am.getMemoryClass() / 4;

            c = Camera.open(cameraId);
            Camera.Parameters parameters = c.getParameters();

            if (mPrefs.getString(Const.PREF_SUPPORTED_PICTURESIZE, "").length() == 0) {
                mImageSizes = new ArrayList<>();
                List<Camera.Size> ls = parameters.getSupportedPictureSizes();

                for (int i=0; i<ls.size(); i++) {
                    Camera.Size s = ls.get(i);
                    if (s.width * s.height <= 1920 * 1080 && s.width * s.height * 4 <= MaxMemory * 1000000)
                        mImageSizes.add(s);
                }
                mPrefs.edit().putString(Const.PREF_SUPPORTED_PICTURESIZE, new Gson().toJson(mImageSizes)).apply();
            } else
                mImageSizes = new Gson().fromJson(mPrefs.getString(Const.PREF_SUPPORTED_PICTURESIZE, ""), new TypeToken<List<Camera.Size>>(){}.getType());
            // Auto focus mode
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes != null && focusModes.size() > 0) {
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            Camera.Size s = mImageSizes.get(0);
            mWidth = s.width; mHeight = s.height;

            parameters.setPictureSize(mWidth, mHeight);
            c.setParameters(parameters);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset(); // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock(); // lock camera for later use
        }
    }

    private boolean prepareMediaRecorder() {
        Option option = mActivity.getOption();

        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_720P);
//        Camera.Size videoSize = mPreview.getVideoSize();
//        if (videoSize != null) {
//            profile.videoFrameWidth = videoSize.width;
//            profile.videoFrameHeight = videoSize.height;
//        } else {
//            profile.videoFrameWidth = mPreview.getPreviewSize().width;
//            profile.videoFrameHeight = mPreview.getPreviewSize().height;
//        }

        mediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(profile);
        Camera.Size videoSize = mPreview.getVideoSize();
        if (videoSize != null) {
            mediaRecorder.setVideoSize(videoSize.width, videoSize.height);
        } else {
            mediaRecorder.setVideoSize(mPreview.getPreviewSize().width, mPreview.getPreviewSize().height);
        }

        mediaRecorder.setOutputFile(mActivity.getConfig().getTempVideo().getAbsolutePath());
        if (option.getVideoLimit() > 0) {
            mediaRecorder.setMaxDuration(mActivity.getOption().getVideoLimit() * 1000);
        } else {
            releaseMediaRecorder();
            Toast.makeText(mActivity, R.string.message_invalid_video_time_limit, Toast.LENGTH_SHORT).show();
            return false;
        }

        // Recorded video orientation should equal to preview orientation
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        WindowManager winManager = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        int rotation = winManager.getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        mediaRecorder.setOrientationHint(result);

        try {
            mediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());
            mediaRecorder.prepare();
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (isSaving)
            return;
        switch (view.getId()) {
            case R.id.video_btnCamera:
                if (mRecording)
                    stopRecording();
                mActivity.changeCameraMode(MainActivity.MODE_PHOTO);
                break;
            case R.id.video_btnPause:
                if (mRecording) {
                    stopRecording();
                    displayVideoThumbnail();
                    confirmSaveVideo();
                }
                break;
            case R.id.video_btnRecording:
                if (!mRecording) {
                    startRecording();
                } else {
                    stopRecording();
                    displayVideoThumbnail();
                    confirmSaveVideo();
                }
                break;
            case R.id.video_imgThumb:
                if (mRecording)
                    stopRecording();
                File media = mActivity.getConfig().getTempVideo();
                if (media.exists()) {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(media), "video/*");
                    startActivity(intent);
                }
                break;
            default:
                break;

        }
    }

    private void createVideoThumbnail(File videoFile) {
        String thumbName = videoFile.getName() + ".jpg";
        File thumb = new File(mActivity.getConfig().getThumbDir(), thumbName);
        if (!thumb.exists()) {
            try {
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                FileOutputStream os = new FileOutputStream(thumb);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
                bitmap.recycle();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void confirmSaveVideo() {
        final int limit = mActivity.getOption().getPhotoLimit();
        if (limit == 0) {
        } else {
            isSaving = true;
            if (getStorageCount() >= limit) {
                File[] files = mActivity.getConfig().getMediaDir().listFiles();
                Arrays.sort(files);
                if (files.length > 0)
                    files[0].delete();
            }
            saveVideoToGallery();
        }
    }

    private void startRecording() {
        if (mRecording)
            return;
        if (!prepareMediaRecorder())
            return;
        try {
            mediaRecorder.setOnInfoListener(this);
            mediaRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        mRecording = true;
        mElapsedTime = 0;
        mTvElapsed.setText("0");
        mElapsedCounter = new ElapsedCounter(mTvElapsed);
        mRecordTimer = new Timer();
        mRecordTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mElapsedTime += 1;
                Message msg = mElapsedCounter.obtainMessage();
                msg.what = ElapsedCounter.UPDATE_TEXT;
                msg.arg1 = mElapsedTime;
                mElapsedCounter.sendMessage(msg);
            }
        }, 1000, 1000);
        mBtnRecord.startAnimation(createButtonAnimation(mBtnRecord, R.drawable.ic_pause));
        mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_video));
    }

    private void displayVideoThumbnail() {
        new AsyncTask<Integer, Integer, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Integer... integers) {
                File file = mActivity.getConfig().getTempVideo();
                if (file.exists())
                    return ThumbnailUtils.createVideoThumbnail(file.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap aBitmap) {
                if (aBitmap != null) {
                    mImgThumb.setImageBitmap(aBitmap);
                    mThumbLayout.setVisibility(View.VISIBLE);
                } else {
                    mThumbLayout.setVisibility(View.INVISIBLE);
                }
            }
        }.execute();
    }

    private void stopRecording() {
        if (!mRecording)
            return;
        try {
            mediaRecorder.stop();
            mRecordTimer.cancel();
            mElapsedCounter.sendEmptyMessage(-1);
            releaseMediaRecorder();
            mElapsedTime = 0;
            mTvElapsed.setText("0");
            mRecording = false;
            mRecordTimer = null;
            mElapsedCounter = null;
            mBtnRecord.startAnimation(createButtonAnimation(mBtnRecord, R.drawable.ic_video));
            mBtnPause.startAnimation(createButtonAnimation(mBtnPause, R.drawable.ic_pause));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Animation createButtonAnimation(final ImageButton button, final int resId) {
        Animation anim = AnimationUtils.loadAnimation(mActivity, R.anim.scale_btn_camera);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                button.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        button.setEnabled(true);
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                button.setImageResource(resId);
            }
        });
        return anim;
    }

    private void saveVideoToGallery() {
        new AsyncTask<Integer, Integer, Boolean>() {
            ProgressDialogFragment dialog;
            @Override
            protected void onPreExecute() {
                dialog = ProgressDialogFragment.newInstance(R.string.message_please_wait, false);
                dialog.show(getFragmentManager());
            }

            @Override
            protected Boolean doInBackground(Integer... integers) {
                String s = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(new Date()) + ".mp4";
                File videoFile = new File(mActivity.getConfig().getMediaDir(), s);
                String des = Utils.copyFile(mActivity.getConfig().getTempVideo().getAbsolutePath(), videoFile.getAbsolutePath());
                if (des != null)
                    createVideoThumbnail(videoFile);
                return des != null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                dialog.dismiss();
                isSaving = false;
//                if (aBoolean)
//                    Toast.makeText(mActivity, R.string.message_save_video_success, Toast.LENGTH_SHORT).show();
//                else
//                    Toast.makeText(mActivity, R.string.message_save_video_failed, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    @Override
    public void onInfo(MediaRecorder mediaRecorder, int i, int i1) {
        if (i == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            stopRecording();
            Toast.makeText(mActivity, R.string.message_video_time_limit_reached, Toast.LENGTH_SHORT).show();
            displayVideoThumbnail();
            confirmSaveVideo();
        }
    }

    private int getStorageCount() {
        File[] files = mActivity.getConfig().getMediaDir().listFiles();
//        int count = 0;
//        for (File file : files) {
//            String path = file.getAbsolutePath();
//            String ext = path.substring(path.lastIndexOf(".")).toLowerCase();
//            if (ext.equals(".jpg"))
//                count += 1;
//        }
//        return count;
        return files.length;
    }


    private static class ElapsedCounter extends Handler {
        private static final int UPDATE_TEXT = 1;
        private WeakReference<TextView> mRefTextView;
        public ElapsedCounter(TextView tv) {
            mRefTextView = new WeakReference<TextView>(tv);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TextView tv = mRefTextView.get();
            if (tv == null)
                return;
            if (msg.what == UPDATE_TEXT) {
                int time = msg.arg1;
                tv.setText(time + "");
            } else {
                tv.setText("0");
            }
        }
    }
}
