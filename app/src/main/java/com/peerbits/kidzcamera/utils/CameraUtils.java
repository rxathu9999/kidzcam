package com.peerbits.kidzcamera.utils;

import android.hardware.Camera;
import android.util.Log;

import java.util.List;

/**
 * Created by Công on 6/18/2015.
 */
public class CameraUtils {

    public static Camera.Size getOptimalSize(List<Camera.Size> sizes, int width, int height) {
        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) width / height;
        if (sizes == null)
            return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
//          Log.d("CameraActivity", "Checking size " + size.width + "w " + size.height + "h");
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - height) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - height);
            }
        }
        // Cannot find the one match the aspect ratio, ignore the requirement

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - height) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - height);
                }
            }
        }
        Log.d("getOptimalSize", optimalSize.width + "x" + optimalSize.height);
        return optimalSize;
    }
}
