package com.peerbits.kidzcamera.widget;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.peerbits.kidzcamera.utils.CameraUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by Công on 6/3/2015.
 */
public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private int mZoom, mMaxZoom;
    private Camera.Size mPreviewSize, mPictureSize, mVideoSize;

    private int mCameraId;

    private ScaleGestureDetector mScaleDetector;

    public CameraSurface(Context context, Camera camera, int cameraId) {
        super(context);

        // Source: http://stackoverflow.com/questions/7942378/android-camera-will-not-work-startpreview-fails
        mCamera = camera;
        mCameraId = cameraId;
        if (mCamera.getParameters().isZoomSupported()) {
            mZoom = mCamera.getParameters().getZoom();
            mMaxZoom = mCamera.getParameters().getMaxZoom();
        } else {
            mZoom = mMaxZoom = -1;
        }

        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                mZoom += (4 * (detector.getScaleFactor() - 1));
                if (mZoom < 0)
                    mZoom = 0;
                if (mZoom > mMaxZoom)
                    mZoom = mMaxZoom;
                Camera.Parameters params = mCamera.getParameters();
                params.setZoom((int) mZoom);
                mCamera.setParameters(params);
                return true;
            }
        });

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setKeepScreenOn(true);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d("surfaceCreated", "surfaceCreated");
        try {
            mCamera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        // preview surface does not exist
        if (mHolder.getSurface() == null)
            return;

        try {
            Camera.Parameters parameters = mCamera.getParameters();

            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(mCameraId, info);

            WindowManager winManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            int rotation = winManager.getDefaultDisplay().getRotation();

            int degrees = 0;

            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break;
                case Surface.ROTATION_90:
                    degrees = 90;
                    break;
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }

            int result;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            } else {  // back-facing
                result = (info.orientation - degrees + 360) % 360;
            }
            parameters.setRotation(result);
            mCamera.setParameters(parameters);
            mCamera.setDisplayOrientation(result);

            Log.d("SurfaceSize", getWidth() + "x" + getHeight());
            parameters = mCamera.getParameters();
            if (mPictureSize != null) {
                parameters.setPictureSize(mPictureSize.width, mPictureSize.height);
                Log.d("mPictureSize", mPictureSize.width + "x" + mPictureSize.height);
            }
            mCamera.setParameters(parameters);
            parameters = mCamera.getParameters();
            if (mPreviewSize != null) {
                parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            }
            try {
                mCamera.setParameters(parameters);
            } catch (Exception e) {
                mPreviewSize = mCamera.getParameters().getPreviewSize();
                requestLayout();
                invalidate();
            }
            Log.d("mPreviewSize", mPreviewSize.width + "x" + mPreviewSize.height);
            try {
//                mCamera.stopPreview();
                mCamera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mPreviewSize = mCamera.getParameters().getPreviewSize();
            Log.d("Actual preview size", mCamera.getParameters().getPreviewSize().width + "x" + mCamera.getParameters().getPreviewSize().height);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mCamera = null;
        }
    }

    public void startCameraPreview() {
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Source:
        // http://stackoverflow.com/questions/7942378/android-camera-will-not-work-startpreview-fails
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        List<Camera.Size> previewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        if (previewSizes != null) {
            mPreviewSize = CameraUtils.getOptimalSize(previewSizes, width, height);
        }
        List<Camera.Size> pictureSizes = mCamera.getParameters().getSupportedPreviewSizes();
        if (pictureSizes != null) {
            mPictureSize = CameraUtils.getOptimalSize(pictureSizes, width, height);
        }
        List<Camera.Size> videoSizes = mCamera.getParameters().getSupportedVideoSizes();
        if (videoSizes != null) {
            mVideoSize = CameraUtils.getOptimalSize(videoSizes, width, height);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d("onLayout", "Layout changed :" + changed + "-" + left + "-" + top + "-" + right + "-" + bottom);
        if (changed) {
            Log.d("onLayout", "Layout changed");
            final int width = right - left;
            final int height = bottom - top;

            int previewWidth = width;
            int previewHeight = height;

            if (mPreviewSize != null) {
                Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE))
                        .getDefaultDisplay();

                if (display.getRotation() != Surface.ROTATION_90 && display.getRotation() != Surface.ROTATION_270) {
                    previewWidth = mPreviewSize.height;
                    previewHeight = mPreviewSize.width;
                } else {
                    previewWidth = mPreviewSize.width;
                    previewHeight = mPreviewSize.height;
                }
            }

            boolean useFirstStrategy = (width * previewHeight < height * previewWidth);
            if (useFirstStrategy) {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
            } else {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mZoom >= 0)
            mScaleDetector.onTouchEvent(event);
        return true;
    }

    public Camera.Size getVideoSize() {
        return mVideoSize;
    }
    public Camera.Size getPreviewSize() {
        return mPreviewSize;
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int width, int height) {
        // Source:
        // http://stackoverflow.com/questions/7942378/android-camera-will-not-work-startpreview-fails
        Camera.Size optimalSize = null;

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) height / width;

        // Try to find a size match which suits the whole screen minus the
        // menu on the left.
        for (Camera.Size size : sizes) {

            if (size.height != width)
                continue;
            double ratio = (double) size.width / size.height;
            if (ratio <= targetRatio + ASPECT_TOLERANCE && ratio >= targetRatio - ASPECT_TOLERANCE) {
                optimalSize = size;
            }
        }

        // If we cannot find the one that matches the aspect ratio, ignore
        // the requirement.
        if (optimalSize == null) {
        }

        return optimalSize;
    }

}
