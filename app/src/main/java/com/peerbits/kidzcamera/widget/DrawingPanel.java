
package com.peerbits.kidzcamera.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Công on 6/8/2015.
 */
public class DrawingPanel extends View {

    public static final int MODE_BRUSH = 0;
    public static final int MODE_ERASER = 1;

    private static final int BRUSH_SIZE = 20;
    private static final int ERASER_SIZE = 60;

    //canvas bitmap
    private Bitmap photoBitmap, pathBitmap;
    private Canvas pathCanvas;
    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private Paint drawPaint, canvasPaint;
    private Paint eraserPaint;
    //initial color
    private int paintColor = Color.RED;

    private int mMode = MODE_BRUSH;

    private List<DrawerState> mStates = new ArrayList<>();
    private List<DrawerState> mUndoStates = new ArrayList<>();

    public DrawingPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupDrawing();
    }

    public DrawingPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupDrawing();
    }

    public DrawingPanel(Context context) {
        super(context);
        setupDrawing();
    }

    //setup drawing
    private void setupDrawing() {
        //prepare for drawing and setup paint stroke properties
        drawPath = new Path();
        drawPaint = createBrushPaint(paintColor);
        canvasPaint = new Paint(Paint.DITHER_FLAG);

        eraserPaint = new Paint();
        eraserPaint.setAlpha(0);
        eraserPaint.setColor(Color.TRANSPARENT);
        eraserPaint.setStrokeWidth(ERASER_SIZE);
        eraserPaint.setStyle(Paint.Style.STROKE);
        eraserPaint.setMaskFilter(null);
        eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        eraserPaint.setAntiAlias(true);
    }

    //size assigned to view
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        photoBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        pathBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        pathCanvas = new Canvas(pathBitmap);
    }

    //draw the view - will be called after touch event
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(photoBitmap, 0, 0, canvasPaint);
        canvas.drawBitmap(pathBitmap, 0, 0, canvasPaint);
    }

    //register user touches as drawing action
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        //respond to down, move and up events
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                if (mMode == MODE_BRUSH) {
                    pathCanvas.drawPath(drawPath, drawPaint);
                }
                if (mMode == MODE_ERASER) {
                    pathCanvas.drawPath(drawPath, eraserPaint);
                }
                break;
            case MotionEvent.ACTION_UP:
                drawPath.lineTo(touchX, touchY);
                if (mMode == MODE_BRUSH) {
                    pathCanvas.drawPath(drawPath, drawPaint);
                    mStates.add(new Brush(drawPath, paintColor));
                }
                if (mMode == MODE_ERASER) {
                    pathCanvas.drawPath(drawPath, eraserPaint);
                    mStates.add(new Eraser(drawPath));
                }
                drawPath = new Path();
                break;
            default:
                return false;
        }
        //redraw
        invalidate();
        return true;
    }

    private Paint createBrushPaint(int color) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(BRUSH_SIZE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        return paint;
    }

    //update color
    public void setColor(int newColor) {
        invalidate();
        paintColor = newColor;
        drawPaint.setColor(paintColor);
    }

    public void setMode(int mode) {
        mMode = mode;
    }

    public void setPhotoBitmap(Bitmap bitmap) {
        if (photoBitmap != null)
            photoBitmap.recycle();
        photoBitmap = bitmap;
        Log.d("bitmap", photoBitmap.getWidth() + "x" + photoBitmap.getHeight());
        Log.d("screen", getWidth() + "x" + getHeight());
        invalidate();
    }

    public void saveTo(File file) {
        try {
            Log.d("saveTo", photoBitmap.getWidth() + "x" + photoBitmap.getHeight());
            Bitmap bitmap = Bitmap.createBitmap(photoBitmap.getWidth(), photoBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(photoBitmap, 0, 0, canvasPaint);
            canvas.drawBitmap(pathBitmap, 0, 0, canvasPaint);
            FileOutputStream os = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
            bitmap.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void undo() {
        if (mStates.size() > 0) {
            mUndoStates.add(mStates.remove(mStates.size() - 1));
            int     w = pathBitmap.getWidth(),
                    h = pathBitmap.getHeight();
            pathBitmap.recycle();
            pathBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            pathCanvas.setBitmap(pathBitmap);
            for (DrawerState state : mStates) {
                if (state.mode == MODE_BRUSH) {
                    pathCanvas.drawPath(((Brush) state).drawPath, createBrushPaint(((Brush) state).paintColor));
                }
                if (state.mode == MODE_ERASER) {
                    pathCanvas.drawPath(((Eraser) state).drawPath, eraserPaint);
                }
            }
            invalidate();
        }
    }

    public void removeEffects() {
        mStates.clear();
        mUndoStates.clear();
        int w = pathBitmap.getWidth(), h = pathBitmap.getHeight();
        pathBitmap.recycle();
        pathBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        pathCanvas.setBitmap(pathBitmap);
        invalidate();
    }

    public void redo() {
        // Not needed
        if (mUndoStates.size() > 0) {
            mStates.add(mUndoStates.remove(mUndoStates.size() - 1));
            int     w = pathBitmap.getWidth(),
                    h = pathBitmap.getHeight();
            pathBitmap.recycle();
            pathBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            pathCanvas.setBitmap(pathBitmap);
            for (DrawerState state : mStates) {
                if (state.mode == MODE_BRUSH) {
                    pathCanvas.drawPath(((Brush) state).drawPath, createBrushPaint(((Brush) state).paintColor));
                }
                if (state.mode == MODE_ERASER) {
                    pathCanvas.drawPath(((Eraser) state).drawPath, eraserPaint);
                }
            }
            invalidate();
        }
    }



    private static class DrawerState {
        protected int mode;
    }

    private static class Brush extends DrawerState {

        private Path drawPath;
        private int paintColor;

        public Brush(Path drawPath, int paintColor) {
            mode = MODE_BRUSH;
            this.drawPath = drawPath;
            this.paintColor = paintColor;
        }
    }

    private static class Eraser extends DrawerState {

        private Path drawPath;

        public Eraser(Path drawPath) {
            mode = MODE_ERASER;
            this.drawPath = drawPath;
        }
    }
}
